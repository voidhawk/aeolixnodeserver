/**
Author: Jürgen Stolz / PTV Group
Copyright: 2018
Research project Aeolix

Description:

History:
20181008 | Created

*/

/**
 * Logging functionality
 */
var logger = require('../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./mongoDB.js => ";

/**
 * Database functionality
 * Verwenden der research-ptv MongoDB(CosmosDB) Datenbank
 */
const mongoose = require('mongoose');
var dbinitialized = false;

module.exports.init = function () {

	log.trace(LOG_FILE_NAME + "init(...) called");
	initializeDB();
}

initializeDB = function() {

	mongoose.connect('mongodb://research-ptv.documents.azure.com:10255/Aeolix?ssl=true', {
		useNewUrlParser: true,
		auth: {
			user: 'research-ptv',
			password: 'dAF2l0NQ8qiCfwNDYS9wjmc4YBUuGRhRFfJhJWQv8LRksUNqedVB0MzUjTcOf7PUEfHUKF6cSOTXhAlbZQCTgg=='
		}
	}).then(() => {

		log.info(LOG_FILE_NAME + "MongoDB connection successfull");
		dbinitialized = true;
		dbQueryUser();
	}).catch(() => {

		log.fatal(LOG_FILE_NAME + "MongoDB connection FAILURE");
	});	
}


dbQueryUser = function() {
	
	log.info(LOG_FILE_NAME + "dbQueryUser(...) called");
}

getSubscriptions = function() {

}