const mongoose = require('mongoose');

const fs = require('fs');
const _ = require('lodash');


var logger = require('../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./Database.js => ";
class Database {

	constructor(dbName, uri, options, modelFolder) {

		this.dbName = dbName;
		this.uri = uri;
		this.options = options;
		this.modelFolder = modelFolder;
		this.connect(this.uri, this.options, modelFolder);
	}

	connect(uri, options, modelFolder) {

		const connection = mongoose.createConnection(uri, options);
		connection.on('error', function (error) {

			log.error(LOG_FILE_NAME + `connect(..) error: ${error}`);
		});

		//var that = this;
		//connection.on('open', onOpenCallback);
		connection.on('open', function () {

			log.info(LOG_FILE_NAME + `connection.on(..) DB: opened`);
		});
		
		fs.readdirSync(`${__dirname}/models/${modelFolder}`).forEach(function (file) {

			if (file.indexOf('.js')) {
				require(`./models/${modelFolder}/${file}`)(connection);
			}
		});

		this.DBConnection = connection;
		function onOpenCallback() {

			console.log("DB: opened");
			//console.log("DB:" + that.dbName +" at " + that.uri + " opened");
		}
	}
}

module.exports = Database;