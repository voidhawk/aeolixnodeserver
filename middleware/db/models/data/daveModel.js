const mongoose = require('mongoose');
const modelName = "tour";
const collectionName = modelName + "s";

const daveSchema = mongoose.Schema({

	type: String,
	creationTime: { type: Date, default: Date.now},
	updateTime: { type: Date, default: Date.now},
	tour: Object,
	tourExecution: Object
}, { collection: collectionName });

module.exports = mongoose.model('DAveTourModel', daveSchema);