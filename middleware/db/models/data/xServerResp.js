const mongoose = require('mongoose');
const modelName = "xserverresponse";
const collectionName = modelName + "s";

const xServerResponseSchema = mongoose.Schema({

	name: String,
	type: String,
	creationTime: { type: Date, default: Date.now},
	routeStartTime: { type: Date},
	request: Object,
	response: Object
}, { collection: collectionName });

module.exports = mongoose.model('XServerResponseModel', xServerResponseSchema);