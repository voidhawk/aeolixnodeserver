/**
Author: Jürgen Stolz / PTV Group
Copyright: 2018
Project: Web application

Description:

History:
20181205 | Created

*/
const mongoose = require('mongoose');

const modelName = "Right";
const collectionName = modelName + "s";
const rightSchema = mongoose.Schema({

	name: String,
	description: { type: String },
	creationTime: { type: Date, default: Date.now },
	updateTime: { type: Date, default: Date.now },
	configs: []
}, { collection: collectionName });

module.exports = mongoose.model('RightModel', rightSchema);