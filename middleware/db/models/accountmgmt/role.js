/**
Author: Jürgen Stolz / PTV Group
Copyright: 2018
Project: Web application

Description:

History:
20181105 | Created

*/
const mongoose = require('mongoose');

const modelName = "Role";
const collectionName = modelName + "s";
const roleSchema = mongoose.Schema({

	name: String,
	creationTime: { type: Date, default: Date.now },
	/**
	 * type => "template" or "config"
	 * ToDo: Brauche ich doch eigentlich nicht, oder doch?
	 */
	type: String,
	rights: [],
	configs: []
}, { collection: collectionName });

module.exports = mongoose.model('RoleModel', roleSchema);