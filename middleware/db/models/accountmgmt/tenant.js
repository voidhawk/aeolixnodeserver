/**
Author: Jürgen Stolz / PTV Group
Copyright: 2018
Project: Web application

Description:

History:
20181026 | Created

*/
const mongoose = require('mongoose');
//var uniqueValidator = require('mongoose-unique-validator');

const modelName = "Tenant";
const collectionName = modelName + "s";

const tenantSchema = mongoose.Schema({

	creationTime: { type: Date, default: Date.now },
	updateTime: { type: Date, default: Date.now },
	tenantId: { type: String },
	name: { type: String },
	description: { type: String },
	rights: [],
	/**
	 *  Externe Ids:
	 * Ein Bsp. dafür "TourToken": "ABCSDWEEWE12345"
	 */
	extIds: [{ key: String, value: String, desc: String }]
}, { collection: collectionName });

tenantSchema.index({
  tenantId: 1,
  name: 1,
}, {
  unique: true,
});
//tenantSchema.plugin(uniqueValidator);

module.exports = mongoose.model('TenantModel', tenantSchema);