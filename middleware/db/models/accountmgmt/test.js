const mongoose = require('mongoose');

const testSchema = mongoose.Schema({

	name: String,
	date: { type: Date, default: Date.now}
//	temp: {type: String, required: true}
});

module.exports = mongoose.model('TestModel', testSchema);


/*
const modelName = "SharingInvitation";
const collectionName = modelName + "s";

const numberOfHoursBeforeExpiry = 24;

var expiryDate = new Date ();
expiryDate.setHours(expiryDate.getHours() + numberOfHoursBeforeExpiry);

var invitationSchema = new Schema({
    // _id: (ObjectId), // Uniquely identifies the invitation (autocreated by Mongo)

    // gives time/day that the invitation will expire
    expiry: { type: Date, default: expiryDate },

    // The user is being invited to share the following:
    owningUser: ObjectId, // The _id of a PSUserCredentials object.
    capabilities: [String] // capability names
}, { collection: collectionName });
*/