/**
Author: Jürgen Stolz / PTV Group
Copyright: 2018
Project: Web application

Description:

History:
20181205 | Created

*/
const mongoose = require('mongoose');

const modelName = "User";
const collectionName = modelName + "s";
const userSchema = mongoose.Schema({

	creationTime: { type: Date, default: Date.now },
	tenantId: String,
	firstName: String,
	lastName: String,
	sign: String,
	email: String,
	pwd: String,
	type: String,
	configs: []
	}, { collection: collectionName });

module.exports = mongoose.model('UserModel', userSchema);
