const mongoose = require('mongoose');
const Database = require('./Database');
const _ = require('lodash');

mongoose.Promise = global.Promise;
const dbMap = new Map();

exports.registerDatabase = (dbName, uri, options, name = 'default') => {

	const database = new Database(dbName, uri, options, name);
	dbMap.set(dbName, database);

	return database;
};

exports.mongoose = (dbName = 'default') => {

	return _.get(dbMap.get(dbName), 'DBConnection');
};