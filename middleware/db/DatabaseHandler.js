var Database = require('./Database');
const _ = require('lodash');

class DatabaseHandler {

	//dbMap = new Map();
	/* 	constructor(dbName, uri, options, modelFolder) {
	
			this.databaseMap = new Map();
			this.dbName = dbName;
			this.uri = uri;
			this.options = options;
			this.modelFolder = modelFolder;
		}
	 */
	constructor() {

			this.databaseMap = new Map();
	}

	registerDatabase(dbName, uri, options, modelFolder = 'default') {

		const database = new Database(dbName, uri, options, modelFolder);
		//database.connect();
		this.databaseMap.set(dbName, database);

		return database;
	}

	getDatabaseConnection(dbName) {

		return _.get(this.databaseMap.get(dbName), 'DBConnection');
	}
}

module.exports = new DatabaseHandler();