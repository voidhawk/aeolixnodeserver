var log4js = require("log4js");
log4js.configure({
	appenders:
	{
		application:
		{
			type: 'dateFile',
			filename: 'application.log',
			//"pattern":"-dd--hh.log",
			"layout": {
				"type": "pattern",
				"pattern": '%d %p - %m'
			}
		}
	},
	categories: { default: { appenders: ['application'], level: 'trace' } }
});
const logger = log4js.getLogger("application");

var getLogger = function () {
	return logger;
};

exports.logger = getLogger();