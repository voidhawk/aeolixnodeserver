/**
Author: Jürgen Stolz / PTV Group
Copyright: 2018

Description:

History:
20181130 | Created

*/
const emitter = require('emitter-io');

class EmitterHandler {

	constructor() {

		this.emitter = emitter.connect();
	}

	publishNotification(channelKey, channelName, subscriptionID, data) {

		/* 		var tempResult = this.emitter.presence({
		
					key: channelKey,
					channel:channelName,
					status: true
				})
				console.log(JSON.stringify(tempResult));
		 */
		var extendedData = {

			key: channelKey,
			channel: `${channelName}/${subscriptionID}/`,
			//message: data
			message: JSON.stringify(data)
		}
		this.emitter.publish(extendedData);
	}
}

module.exports = EmitterHandler;
