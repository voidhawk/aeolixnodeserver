/**
Author: Jürgen Stolz / PTV Group
Copyright: 2018
Research project Aeolix

Description:

History:
20181016 | Created

*/

var request = require('request');
var logger = require('../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./dave-api.js => ";
const axios = require('axios');

/* module.exports.createTour = function (tour, url, originator, uniqueID, callback) {

	log.info(LOG_FILE_NAME + "createTour(..) called");
	//logger.info({ info: 'dave-api.js=>createTour()', uniqueID: uniqueID, originator: originator });
	var options = {
		url: url,
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		json: tour
	};

	request(options, function (err, response, body) {

		if (!err) {

			log.info(LOG_FILE_NAME + "createTour(..) SUCCESS");
			log.info(LOG_FILE_NAME + "created trip => " + JSON.stringify(body));
			callback(body, originator, uniqueID);

			return body;
		} else {

			//logger.info({ info: 'dave-api.js=>createTour()', status: 'ERROR', uniqueID: uniqueID, content: err });
			log.error(LOG_FILE_NAME + "createTour(..) FAILURE");
			callback(body, originator, uniqueID);

			return null;
		}
	});
}
 */
/* const executeDeleteTour = async (url, options) => {

	try {
    const response = await axios.delete(url);
//    const data = response.data;
		console.log(response);
		return response;
  } catch (error) {
		console.log(error);
		return error.response;
  }}

module.exports.deleteTour = async function (url) {

	log.info(LOG_FILE_NAME + `deleteTour(..) called. URL=${url}`);
	return await executeDeleteTour(url);
}
 */
module.exports.createPosEvent = function (posEvent, url, originator, uniqueID, callback) {

	log.info(LOG_FILE_NAME + "createPosEvent(..) called");
	//logger.info({ info: 'dave-api.js=>createPosEvent()', uniqueID: uniqueID, originator: originator });
	var options = {
		url: url,
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		json: posEvent
	};

	request(options, function (err, response, body) {

		if (!err) {

			log.info(LOG_FILE_NAME + "createPosEvent(..) SUCCESS");
			log.info(LOG_FILE_NAME + "created createPosEvent => " + JSON.stringify(body));
			//logger.info({ info: 'dave-api.js=>createPosEvent()', status: 'SUCCESS', uniqueID: uniqueID });
			callback(body, originator, uniqueID);
			return body;
		} else {
			//logger.error({ info: 'dave-api.js=>createPosEvent()', status: 'ERROR', uniqueID: uniqueID, content: err });
			log.error(LOG_FILE_NAME + "createPosEvent(..) FAILURE");
			callback(body, originator, uniqueID);
			return null;
		}
	});
}
