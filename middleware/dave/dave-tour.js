/**
Author: Jürgen Stolz / PTV Group
Copyright: 2018
Research project Aeolix

Description:

History:
20181122 | Created

*/

var logger = require('../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./dave-tour.js => ";

//import axios from 'axios'; //Typescript?
var axios = require('axios');

class DAveTourAPI {

	constructor() {

	}

	/**
	 * Call this method and process the returned result.
	 * @param {*} url 
	 * @param {*} tripData 
	 */
	async createTrip(url, tripData) {

		var result = {

			error: true
		}
		await this.execCreateDAveTrip(url, tripData)
			.then(response => {
				log.info(`${LOG_FILE_NAME}execCreateDAveTrip(..) SUCCESS: ${JSON.stringify(response.data)}`);
				if (response.data.responseStatus.errorCode === "SUCCESS") {
					result.error = false;
					result["tour"] = response.data.tour;
				}
			})
			.catch(err => {
				log.error(`${LOG_FILE_NAME}execCreateDAveTrip(..) FAILURE: ${err}`);
				result.error = true;
				result.msg = "Error";
				result.err = err;
			});
		return result;
	}

	async execCreateDAveTrip(url, tripData) {

		log.info(LOG_FILE_NAME + "execCreateDAveTrip(..): " + JSON.stringify(tripData));

		var options = {
			method: 'POST',
			url: url,
			data: tripData,
			headers: {
				'Content-Type': 'application/json'
			},
			json: true
		};
		return await axios(options);
	}

	async deleteTrip(url) {

		var result = {

			error: true
		}
		await this.execDeleteDAveTrip(url)
			.then(response => {
				log.info(`${LOG_FILE_NAME}execDeleteDAveTrip(..) SUCCESS: ${JSON.stringify(response.data)}`);
				if (response.data.responseStatus.errorCode === "SUCCESS") {
					result.error = false;
				}
			})
			.catch(err => {
				log.error(`${LOG_FILE_NAME}execDeleteDAveTrip(..) FAILURE: ${JSON.stringify(err.response.data)}`);
				result.msg = "Error";
			});
		return result;
	}

	async execDeleteDAveTrip(url) {

		log.info(LOG_FILE_NAME + `execDeleteDAveTrip(${url}):`);

		var options = {
			method: 'DELETE',
			url: url,
			headers: {
				'Content-Type': 'application/json'
			},
			json: true
		};
		return await axios(options);
	}

	async createSubscription(url, subscription) {

		log.info(LOG_FILE_NAME + "createSubscription(..) called");
		var result = {

			error: true
		}
		await this.execCreateDAveSubscription(url, subscription)
			.then(response => {
				log.info(`${LOG_FILE_NAME}execCreateDAveSubscription(..) SUCCESS: ${JSON.stringify(response.data)}`);
				if (response.data.responseStatus.errorCode === "SUCCESS") {
					result.error = false;
					result["subscription"] = response.data;
				}
			})
			.catch(err => {
				log.error(`${LOG_FILE_NAME}execCreateDAveSubscription(..) FAILURE: ${JSON.stringify(err.response.data)}`);
				result.error = true;
				result.msg = "Error";
			});
		return result;
	}

	async execCreateDAveSubscription(url, subscription) {

		log.info(LOG_FILE_NAME + "execCreateDAveSubscription(..): " + JSON.stringify(subscription));

		var options = {
			method: 'POST',
			url: url,
			data: subscription,
			headers: {
				'Content-Type': 'application/json'
			},
			json: true
		};
		return await axios(options);
	}

	async createPosition(url, position) {

		log.info(LOG_FILE_NAME + "createPosition(..) called");
		var result = {

			error: true
		}
		await this.execCreateDAvePosition(url, position)
			.then(response => {
				
				log.info(`${LOG_FILE_NAME}execCreateDAvePosition(..) SUCCESS: ${JSON.stringify(response.data)}`);
				if (response.data.responseStatus.errorCode === "SUCCESS") {
					result.error = false;
					result["position"] = response.data;
				}
			})
			.catch(err => {

				log.error(`${LOG_FILE_NAME}execCreateDAvePosition(..) FAILURE: ${JSON.stringify(err.response.data)}`);
				result.error = true;
				result.msg = "Error";
			});
		return result;
	}

	async execCreateDAvePosition(url, position) {

		log.info(LOG_FILE_NAME + "execCreateDAvePosition(..): " + JSON.stringify(position));

		var options = {
			method: 'POST',
			url: url,
			data: position,
			headers: {
				'Content-Type': 'application/json'
			},
			json: true
		};
		return await axios(options);
	}
}

module.exports = DAveTourAPI;