/**
Author: Jürgen Stolz / PTV Group
Copyright: 2019
Research project Aeolix

Description:

History:
20190507 | Created

*/

var logger = require('../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./theHaulier-order-api.js => ";

//import axios from 'axios'; //Typescript?
var axios = require('axios');

class TheHaulierOrderAPI {

	constructor() {

	}

	/**
	 * Call this method and process the returned result.
	 * @param {*} url 
	 * @param {*} singleOrder 
	 */
	async createOrder(url, extOrder) {

		var result = {

			error: true
		}
		await this.execCreateTheHaulierOrder(url, extOrder)
			.then(response => {
				log.info(`${LOG_FILE_NAME}execCreateTheHaulierOrder(..) SUCCESS: ${JSON.stringify(response.data)}`);
				result.error = response.data.error;
			})
			.catch(err => {
				log.error(`${LOG_FILE_NAME}execCreateTheHaulierOrder(..) FAILURE: ${err}`);
				result.error = true;
				result.msg = "Error";
				result.err = err;
			});
		return result;
	}

	async execCreateTheHaulierOrder(url, extOrder) {

		log.info(LOG_FILE_NAME + `execCreateTheHaulierOrder(..) called`);

		var options = {
			method: 'POST',
			url: url,
			data: extOrder,
			headers: {
				'Content-Type': 'application/json'
			},
			json: true
		};
		return await axios(options);
	}
}

module.exports = TheHaulierOrderAPI;