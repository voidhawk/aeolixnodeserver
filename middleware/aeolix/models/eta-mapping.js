/**
Author: Jürgen Stolz / PTV Group
Copyright: 2018
Project: Aeolix

Description:

History:
20181026 | Created

*/
const mongoose = require('mongoose');

const etaMapping = mongoose.Schema({

	scemid: String,
	clientId: String,
	subscriptionId: String,
	date: { type: Date, default: Date.now },
	// Addresse
	// Ansprechpartner
	// Gültigkeit
	/**
	 *  Externe Ids:
	 * Ein Bsp. dafür "TourToken": "ABCSDWEEWE12345"
	 */ 
	extIds: [{ key: string, value: String, desc: String }]
});

module.exports = mongoose.model('Mnemonic', mnemonicSchema);