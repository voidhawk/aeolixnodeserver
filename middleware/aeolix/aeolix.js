/**
Author: Jürgen Stolz / PTV Group
Copyright: 2018
Research project Aeolix

Description:

History:
20181009 | Created

*/

var logger = require('../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./aeolix.js => ";

/**
 * DAve
 */
var DAveTourAPI = require('../dave/dave-tour.js');

/**
 * TheHaulier
 */
var TheHaulierOrderAPI = require('../theHaulier/theHaulier-order-api.js');

//Produktive
/*
const DS_ETA_TRIP_ID = "5b9f47fce661910005a07de6";
const DS_ETA_SERVICE_ID = "5b9b87e8aee25e0005233abe";
const DS_ETA_POSITION_ID = "5b9f4827e661910005a07de7";
const DS_ETA_SUBSCRIPTION_ID = "5b9f4857e661910005a07de8";
const DS_ETA_NOTIFICATION_ID = "5bc6f957dcc514001bd7dd71";
const SRV_ETAService_Trip = "5bf51f60624f340015727477"; //SRV_ETAService_Trip
*/

/**
 * Development Einstellungen
 */
//Datasources
const DS_ETA_TRIP_ID = "5bc6c312dcc514001bd7dd4e";
const DS_ETA_SERVICE_ID = "5bc6c2dddcc514001bd7dd4c";
const DS_ETA_POSITION_ID = "5bc6c332dcc514001bd7dd4f";
const DS_ETA_SUBSCRIPTION_ID = "5bc6c2fddcc514001bd7dd4d";

/**
 * Productive Cloud Server (https://research-api.cloud.ptvgroup.com, research-ptv-srv)
 */
/**
// DataSource
//const DS_ETA_NOTIFICATION_ID = "5bc6f957dcc514001bd7dd71"; //ETANotification
//Service
//const SRV_ETAService_Trip = "5bf51f60624f340015727477"; //SRV_ETAService_Trip
 */

/**
 * Development Productive (NodeJS) Cloud Server (https://research-ptv-test.azurewebsites.net, research-ptv-test)
 */
// DataSource
const DS_ETA_NOTIFICATION_ID = "5bc6ed8adcc514001bd7dd6a"; //Dev-Notification
//Service
// => das war der "alte" const SRV_ETAService_Trip = "5bfb9f00624f3400157274ba"; //Dev_SRV_ETAService_Trip
const SRV_ETAService_Trip = "5d9c97c3ad60ee0017a323a6"; //Dev_SRV_ETAService_Trip

/** 
 * Development Local Server
 */
// DataSource
//const DS_ETA_NOTIFICATION_ID = "5c32f18756162d001379fed5"; //Dev_Local_Notification
//Service
// => das war der "alte" const SRV_ETAService_Trip = "5c2db64156162d001379fec9"; //Dev_Local_SRV_ETAService_Trip
//const SRV_ETAService_Trip = "5d9c93d9ad60ee0017a3239c"; //Dev_Local_SRV_ETAService_Trip

//var sdk;
//var datasourceIDs = new Array(DS_ETA_TRIP_ID, DS_ETA_SERVICE_ID, DS_ETA_POSITION_ID, DS_ETA_SUBSCRIPTION_ID, DS_ETA_NOTIFICATION_ID);
var datasourceIDs = new Array(DS_ETA_NOTIFICATION_ID);

var subscriptionContainer;

var SDKWrapper = require('./sdk-wrapper/sdk-wrapper.js');

var initSdkDatasourceWrapper = true;
var sdkDatasourceWrapper = null;

var initSdkRequestWrapper = true;
var sdkRequestWrapper = null;

var initSdkResponseWrapper = true;
var sdkResponseWrapper = null;

var daveDBConnection = null;

function repeatAsync(func, interval, success, error) {
	(function loop() {
		func(
			() => {
				success();
				setTimeout(loop, interval);
			},
			() => {
				error();
				setTimeout(loop, interval);
			}
		);
	})();   // invoke immediately
}

const wait = ms => new Promise(resolve => setTimeout(resolve, ms))
const keepSDKAlive = async (timer) => {

	//await wait(timer);
	while (true) {

		try {

			await wait(timer);
			if (sdkDatasourceWrapper != null) {
				await sdkDatasourceWrapper.refreshAuthToken()
					.then(response => {
						if (log.isDebugEnabled) {

							log.debug(`${LOG_FILE_NAME}refreshAuthToken(sdkDatasourceWrapper) SUCCESSFUL`);
						}
						sdkDatasourceWrapper.setTokens(response.data.access_token, response.data.refresh_token);
					})
					.catch(err => {

						log.error(`${LOG_FILE_NAME}refreshAuthToken(sdkDatasourceWrapper) FAILURE: ${JSON.stringify(err.response.data)}`);
						/**
						 * JSt: ToDo: Dieser Fehler tritt auf, was soll ich tun?
						 * 
						 * Dieses return im await bringt nichts
						 */
						reset();
						return;
					});
			}

			if (sdkRequestWrapper != null) {

				await sdkRequestWrapper.refreshAuthToken()
					.then(response => {
						if (log.isDebugEnabled) {

							log.debug(`${LOG_FILE_NAME}refreshAuthToken(sdkRequestWrapper) SUCCESSFUL`);
						}
						sdkRequestWrapper.setTokens(response.data.access_token, response.data.refresh_token);
					})
					.catch(err => {
						log.error(`${LOG_FILE_NAME}refreshAuthToken(sdkRequestWrapper) FAILURE: ${JSON.stringify(err.response.data)}`);
						/**
						 * ToDo: Dieser Fehler tritt auf, was soll ich tun?
						 */
						return;
					});
			}

			if (sdkResponseWrapper != null) {

				await sdkResponseWrapper.refreshAuthToken()
					.then(response => {

						if (log.isDebugEnabled) {

							log.debug(`${LOG_FILE_NAME}refreshAuthToken(sdkResponseWrapper) SUCCESSFUL`);
						}
						sdkResponseWrapper.setTokens(response.data.access_token, response.data.refresh_token);
					})
					.catch(err => {
						log.error(`${LOG_FILE_NAME}refreshAuthToken(sdkResponseWrapper) FAILURE: ${JSON.stringify(err.response.data)}`);
						/**
						 * ToDo: Dieser Fehler tritt auf, was soll ich tun?
						 */
						return;
					});
			}
		} catch (e) {

			/**
			 * ToDo: Egal wir machen weiter, eventuell am SDK ein Flag setzen, dass wir uns neu anmelden müssen
			 */
			log.fatal(`${LOG_FILE_NAME}refreshAuthToken(..) FAILURE: ${JSON.stringify(e)}`);
		}
	}
}

module.exports.socket_control_callback = function socket_control_callback(sdkSubscriptionWrapper, sdkCtrlData) {

	log.info(LOG_FILE_NAME + `socket_control_callback(${sdkSubscriptionWrapper.type}) called: TypeID=${sdkSubscriptionWrapper.typeID}, msg=${JSON.stringify(sdkCtrlData)}`);

	if (sdkCtrlData.code === 200) {

		if (sdkCtrlData.msg != undefined && sdkCtrlData.msg != null) {

			if (sdkCtrlData.msg === "Unsubscribed.") {

				sdkSubscriptionWrapper.socket = null;
				/**
				 * Überprüfen wer das getriggert hat, wenn wir das selbst waren, dann KEINEN Subscribe mehr ausführen
				 */
				if (sdkSubscriptionWrapper.forcedUnsubscribe === false) {

					/**
					 * Info weiterleiten, dass wir uns neu subscriben müssen, das SDK hat uns abgemeldet
					 */
					sdkSubscriptionWrapper.subscribe();
				}
			}
			else {

				/**
				 * Leerer ELSE Zweig ist Absicht: Wir haben das getriggered also gibt es keinen subscribe :) von unserer Seite
				 */
			}
		}
	}
	else if (sdkCtrlData.code === 201) {

		if (sdkCtrlData.msg != undefined && sdkCtrlData.msg != null) {

			if (sdkCtrlData.msg == "Unsubscribed.") {

				if (!sdkSubscriptionWrapper.forcedUnsubscribe) {

					sdkSubscriptionWrapper.subscribe();
				}
			}
		}
		else {

			log.error(LOG_FILE_NAME + `socket_control_callback(${sdkSubscriptionWrapper.type}): TypeID=${sdkSubscriptionWrapper.typeID}, Subscription failed.`);
		}
	}
}

/**
 * In this (socket)callback, we receive every ServiceRequest. Internally we split into different
 * actions like Tour, Subscription
 */
module.exports.socket_request_service_data_callback = async function socket_request_service_data_callback(data) {

	if (log.isDebugEnabled) {

		log.debug(LOG_FILE_NAME + `socket_request_service_data_callback(..) called: Data=${JSON.stringify(data)}`);
	}
	var forward2UserID = data.userID;
	if (data.data.service != null) {

		if (data.data.service === 'DAve') {

			/**
			 * ToDo: ev. über setTimeout verschieben?
			 */
			log.info(LOG_FILE_NAME + `socket_request_service_data_callback(${data.type}), TypeID=${data.typeID}: content=${data.data.service}.${data.data.subType}`);
			/**
			 * ToDo: alles in Richtung DAve auf Promises umbauen
			 */
			if (data.data.subType === "Tour") {

				var url = data.data.data.url;
				url += "?token=" + data.data.data.token;
				url += "&source=" + data.data.data.source;
				var tour = {
					"tour": data.data.data.tour
				}

				var daveTourApi = new DAveTourAPI();
				var response = await daveTourApi.createTrip(url, tour);
				if (response.error === true) {

					log.error(LOG_FILE_NAME + `socket_request_service_data_callback(..) createTrip(..) result=${response.err}`);
					return;
				}
				if (log.isDebugEnabled) {

					log.debug(LOG_FILE_NAME + `socket_request_service_data_callback(..) createTrip(..) result=${JSON.stringify(response)}`);
				}
				//var forward2UserID = data.userID;
				var forwardData = {
					"uniqueID": data.data.uniqueID,
					"service": "DAve",
					"subType": "Tour_Response",
					"data": response.tour
				}
				var dave = {
					data: JSON.stringify(forwardData)
				}

				await sdkRequestWrapper.publish(SRV_ETAService_Trip, forward2UserID, dave)
					.then(response => {
						log.info(`${LOG_FILE_NAME}publishTrip(sdkRequestWrapper) SUCCESS: ${JSON.stringify(response.data)}`);
					})
					.catch(err => {
						log.error(`${LOG_FILE_NAME}publishTrip(sdkRequestWrapper) FAILURE: ${JSON.stringify(err.response)}`);
						return;
					});
			}
			else if (data.data.subType === "Tour_Delete") {

				//http://172.23.112.117:80/em/tour/batch?SCEMIDS=7Y1L9OMOK7&Token=FHYHKGRQ7ZYPVKGXDSPIGKBBS&Source=JSt
				var url = data.data.data.url;
				url += "?SCEMIDS=" + data.data.data.scemids;
				url += "&Token=" + data.data.data.token;
				url += "&Source=" + data.data.data.source;

				var daveTourApi = new DAveTourAPI();
				var response = await daveTourApi.deleteTrip(url);
				if (response.error === true) {

					log.error(LOG_FILE_NAME + `socket_request_service_data_callback(..) deleteTrip(..) result=${response.err}`);
					return;
				}
				if (log.isDebugEnabled) {

					log.debug(LOG_FILE_NAME + `socket_request_service_data_callback(..) deleteTrip(..) result=${JSON.stringify(response)}`);
				}
				//var forward2UserID = data.userID;
				var forwardData = {
					"uniqueID": data.data.uniqueID,
					"service": "DAve",
					"subType": "Tour_Delete_Response",
					"data": response
				}
				var dave = {
					data: JSON.stringify(forwardData)
				}

				await sdkRequestWrapper.publish(SRV_ETAService_Trip, forward2UserID, dave)
					.then(response => {
						log.info(`${LOG_FILE_NAME}publishTrip(sdkRequestWrapper) SUCCESS: ${JSON.stringify(response.data)}`);
					})
					.catch(err => {
						log.error(`${LOG_FILE_NAME}publishTrip(sdkRequestWrapper) FAILURE: ${JSON.stringify(err.response)}`);
						return;
					});
			}
			else if (data.data.subType === 'Subscription') {

				//var forward2UserID = data.userID;
				var url = data.data.data.url;
				url += "?source=" + "NodeJS_Middleware";
				if (data.data.data.notificationReceiverType === "Service") {

					var notificationURL = `${data.data.data.subscription.notificationDescription.callBackURL}?type=${data.data.data.notificationReceiverType}&serviceID=${data.typeID}&userID=${forward2UserID}`;
					data.data.data.subscription.notificationDescription.callBackURL = notificationURL;
				}
				else if (data.data.data.notificationReceiverType === "Datasource") {

					var notificationURL = `${data.data.data.subscription.notificationDescription.callBackURL}?type=${data.data.data.notificationReceiverType}&datasourceID=${data.data.data.myAeolixId}`;
					data.data.data.subscription.notificationDescription.callBackURL = notificationURL;
				}
				var subscriptionContent = {
					"subscription": data.data.data.subscription
				}

				var daveTourApi = new DAveTourAPI();
				var response = await daveTourApi.createSubscription(url, subscriptionContent);
				if (log.isDebugEnabled) {

					log.debug(LOG_FILE_NAME + `socket_request_service_data_callback(..) createSubscription(..) result=${JSON.stringify(response)}`);
				}
				var forwardData = {

					"uniqueID": data.data.uniqueID,
					"service": "DAve",
					"subType": "Subscription_Response",
					"subscription": data.data.data.subscription,
					"subscriptionType": data.data.data.type,
					"scemid": response.subscription.scemid,
					"subscriptionID": response.subscription.subscriptionID
				}
				var dave = {
					data: JSON.stringify(forwardData)
				}

				await sdkRequestWrapper.publish(SRV_ETAService_Trip, forward2UserID, dave)
					.then(response => {
						log.info(`${LOG_FILE_NAME}publishSubscription(sdkRequestWrapper) SUCCESS: ${JSON.stringify(response.data)}`);
					})
					.catch(err => {
						log.error(`${LOG_FILE_NAME}publishSubscription(sdkRequestWrapper) FAILURE: ${JSON.stringify(err.response)}`);
						return;
					});
			}
			else if (data.data.subType === 'PosEvent') {

				var url = data.data.data.url;
				url += "?source=" + data.data.data.source;
				var positionContent = {
					"event": data.data.data.posEvent
				}

				var daveTourApi = new DAveTourAPI();
				var response = await daveTourApi.createPosition(url, positionContent);
				if (log.isDebugEnabled) {

					log.debug(LOG_FILE_NAME + `socket_request_service_data_callback(..) createPosition(..) result=${JSON.stringify(response)}`);
				}

				var forward2UserID = data.userID;
				var forwardData = {
					"uniqueID": data.data.uniqueID,
					"service": "DAve",
					"subType": "Position_Response",
					"position": data.data.data.position
/* 				"scemid": response.subscription.scemid,
				"subscriptionID": response.subscription.subscriptionID
 */			}
				var dave = {
					data: JSON.stringify(forwardData)
				}

				await sdkRequestWrapper.publish(SRV_ETAService_Trip, forward2UserID, dave)
					.then(response => {
						log.info(`${LOG_FILE_NAME}publishSubscription(sdkRequestWrapper) SUCCESS: ${JSON.stringify(response.data)}`);
					})
					.catch(err => {
						log.error(`${LOG_FILE_NAME}publishSubscription(sdkRequestWrapper) FAILURE: ${JSON.stringify(err.response.data)}`);
						return;
					});
			}
			else {

				log.error(LOG_FILE_NAME + `socket_request_service_data_callback(subType=${data.data.subType}) called: SubType is not supported not processed`);
			}
		}
		else if (data.data.service === 'RO_WebService') {

			log.info(LOG_FILE_NAME + `socket_request_service_data_callback(${data.type}), TypeID=${data.typeID}: content=${data.data.service}.${data.data.subType}`);
			if (data.data.subType === 'orders') {

				var url = data.data.url;
				var extOrders = data.data.data.orders;
				log.info(LOG_FILE_NAME + `socket_request_service_data_callback(${data.type}), order count=${extOrders.length}, url=${data.data.url}`);

				// JSt: ToDo: Hier rufen wir einen Schnittstelle an the Haulier auf
				var theHaulierOrderAPI = new TheHaulierOrderAPI();
				var response = await theHaulierOrderAPI.createOrder(url, extOrders);
				/* 				var daveTourApi = new DAveTourAPI();
								var response = await daveTourApi.createPosition(url, positionContent);
				 */
				/* 				
				if (log.isDebugEnabled) {
				
					log.debug(LOG_FILE_NAME + `socket_request_service_data_callback(..) createPosition(..) result=${JSON.stringify(response)}`);
					}
				 */
				/* 			
					Wir liefern im Moment keinen Response an den Sender zurück
					var forward2UserID = data.userID;
					var forwardData = {
						"uniqueID": data.data.uniqueID,
						"service": "DAve",
						"subType": "Position_Response",
						"position": data.data.data.position
					}
					var dave = {
						data: JSON.stringify(forwardData)
					}
	
					await sdkRequestWrapper.publish(SRV_ETAService_Trip, forward2UserID, dave)
						.then(response => {
							log.info(`${LOG_FILE_NAME}publishSubscription(sdkRequestWrapper) SUCCESS: ${JSON.stringify(response.data)}`);
						})
						.catch(err => {
							log.error(`${LOG_FILE_NAME}publishSubscription(sdkRequestWrapper) FAILURE: ${JSON.stringify(err.response.data)}`);
							return;
						});
				 */
			}
		}
	}
	else {

		log.error(LOG_FILE_NAME + `socket_request_service_data_callback(..) called: Parameter service is not valid. message not processed`);
	}
}

module.exports.socket_response_service_data_callback = function socket_response_service_data_callback(data) {

	log.info(LOG_FILE_NAME + `socket_response_service_data_callback(..) called: Data=${JSON.stringify(data)}`);
	if (data.error === false && data.data.service === "DAve") {

		/**
		 * ToDo: diese Inhalte in eine/die DAve Klasse verschieben!!!
		 */
		var daveTourModel = daveDBConnection.model("DAveTourModel");
		if (data.data.subType === "Tour_Response") {

			var tour = data.data.data;
			var tourExecution = {

				currentState: "NEW",
				stopSchedules: []
			}
			tour.stops.forEach(function (stop) {

				var stopScheduleData = {

					scemid: stop.scemid,
					planned: stop.earliestArrivalTime,
					eta: "-",
					currentState: "UNDEFINED"
				};
				tourExecution.stopSchedules.push(stopScheduleData);
			}.bind(this));

			daveTourModel.create({
				"type": data.data.subType,
				"updateTime": new Date().getTime(),
				"tour": tour,
				"tourExecution": tourExecution
			}).then((savedObject) => {
				console.log("DAveTourModel: " + savedObject._id);
			}).catch(() => {
				console.log("research-ptv: MongoDB create save tour FAILURE");
				res.status(400).json({ message: 'Create and save tour in DB failed' });
				return;
			});
		}
		else if (data.data.subType === "Subscription_Response") {

			var typeParam;
			var finder = {};
			if (data.data.subscriptionType === "Trip") {
				typeParam = "tour.scemid"
			}
			else if (data.data.subscriptionType === "Stop") {

				typeParam = "tour.stops.scemid";
			}
			finder[typeParam] = data.data.scemid;
			daveTourModel.findOneAndUpdate(finder,
				{
					'updateTime': new Date(),
					$addToSet: {
						'tour.subscriptions':
						{
							'receivedTime': Date.now,
							'scemid': data.data.scemid,
							'subscription': data.data.subscription,
							'subscriptionID': data.data.subscriptionID
						}
					}
				},
				{
					new: true                       // return updated doc
					//runValidators: true              // validate before update
				}).then(doc => {
					// console.log(doc)
				})
				.catch(err => {
					console.error(err)
				});
		}
	}

	return;
}

module.exports.socket_datasource_data_callback = function socket_datasource_data_callback(data) {

	log.info(LOG_FILE_NAME + `socket_datasource_data_callback(..) called: Data=${JSON.stringify(data)}`);
}

module.exports.setDatabase = async function (dbConnection) {

	daveDBConnection = dbConnection;
}

/**
 * Initialisierung
 */
module.exports.init = async function () {

	try {

		/**
		 * ToDo: das sollte aus einer Datenbank kommen
		 */
		var user = {
			"username": "juergen.stolz@ptvgroup.com",
			"password": "VoidhawkBeiAtos",
			"type": "user"
		};

		if (initSdkDatasourceWrapper === true) {

			sdkDatasourceWrapper = new SDKWrapper();
			await sdkDatasourceWrapper.login2Aeolix(user)
				.then(response => {
					log.info(`${LOG_FILE_NAME}login2Aeolix(sdkDatasourceWrapper) SUCCESSFUL`);
					sdkDatasourceWrapper.setTokens(response.data.access_token, response.data.refresh_token);
				})
				.catch(err => {
					log.error(`${LOG_FILE_NAME}login2Aeolix(sdkDatasourceWrapper) FAILURE: ${err}`);
					sdkDatasourceWrapper = null;

					/*
					login2Aeolix(sdkDatasourceWrapper) FAILURE: Error: certificate has expired

					JSt: ToDo: Der return bringt nichts, die Implementierung ändern!
					*/
					return;
				});
		}

		if (initSdkRequestWrapper === true) {

			sdkRequestWrapper = new SDKWrapper();
			await sdkRequestWrapper.login2Aeolix(user)
				.then(response => {
					log.info(`${LOG_FILE_NAME}login2Aeolix(sdkRequestWrapper) SUCCESSFUL`);
					sdkRequestWrapper.setTokens(response.data.access_token, response.data.refresh_token);
				})
				.catch(err => {
					log.error(`${LOG_FILE_NAME}login2Aeolix(sdkRequestWrapper) FAILURE: ${err}`);
					sdkRequestWrapper = null;
					return;
				});
		}

		if (initSdkResponseWrapper === true) {

			sdkResponseWrapper = new SDKWrapper();
			await sdkResponseWrapper.login2Aeolix(user)
				.then(response => {
					log.info(`${LOG_FILE_NAME}login2Aeolix(sdkResponseWrapper) SUCCESSFUL`);
					sdkResponseWrapper.setTokens(response.data.access_token, response.data.refresh_token);
				})
				.catch(err => {
					log.error(`${LOG_FILE_NAME}login2Aeolix(sdkResponseWrapper) FAILURE: ${err}`);
					sdkResponseWrapper = null;
					return;
				});
		}
		/**
		* keepSDKAlive wird dazu verwendet alle sdks am Leben halten
		*/
		keepSDKAlive(1 * 60 * 1000);

		if (sdkDatasourceWrapper != null) {

			if (datasourceIDs.length > 0) {

				datasourceIDs.forEach(function (datasourceId) {

					sdkDatasourceWrapper.subscribe("Datasource", datasourceId, this.socket_control_callback, this.socket_datasource_data_callback);
				}.bind(this));
			}
		}
		/**
		 * Used to receive data from a client which triggered a request.
		 * PUBLISH 2 a service
		 */
		if (sdkRequestWrapper != null) {

			sdkRequestWrapper.subscribe("ServiceRequest", SRV_ETAService_Trip, this.socket_control_callback, this.socket_request_service_data_callback);
		}

		/**
		 * Used to receive the response from a service followed after a service request
		 */
		if (sdkResponseWrapper != null) {

			sdkResponseWrapper.subscribe("ServiceResponse", SRV_ETAService_Trip, this.socket_control_callback, this.socket_response_service_data_callback);
		}
	} catch (e) {

		log.fatal(LOG_FILE_NAME + "init(..): " + e);
		return;
	}
}

module.exports.reset = async function () {

	try {

		if (initSdkDatasourceWrapper === true) {

			if (sdkDatasourceWrapper != null) {

				sdkDatasourceWrapper.logout();
				sdkDatasourceWrapper = null;
			}
		}

		if (initSdkRequestWrapper === true) {

			if (sdkRequestWrapper != null) {

				sdkRequestWrapper.logout();
				sdkRequestWrapper = null;
			}
		}

		if (initSdkResponseWrapper === true) {

			if (sdkResponseWrapper != null) {

				sdkResponseWrapper.logout();
				sdkResponseWrapper = null;
			}
		}
	} catch (e) {

		log.fatal(LOG_FILE_NAME + "reset(..): " + e);
		return;
	}
}

module.exports.publishNotification2Service = async function (myAeolixId, userId, data) {

	log.info(LOG_FILE_NAME + `publishNotification2Service(MyAeolixId=${myAeolixId}, UserId=${userId})`);

	if (sdkResponseWrapper) {

		await sdkResponseWrapper.publish(myAeolixId, userId, data)
			.then(response => {
				log.info(`${LOG_FILE_NAME}publishNotification2Service(sdkResponseWrapper) SUCCESS: ${JSON.stringify(response.data)}`);
				return;
			})
			.catch(err => {
				log.error(`${LOG_FILE_NAME}publishNotification2Service(sdkResponseWrapper) FAILURE: ${JSON.stringify(err.response.data)}`);
				return;
			});
	}
}

module.exports.publish2Datasource = async function (datasourceId, data) {

	log.info(LOG_FILE_NAME + `publish2Datasource(${datasourceId})`);

	if (sdkDatasourceWrapper) {

		await sdkDatasourceWrapper.publish(DS_ETA_NOTIFICATION_ID, null, data)
			.then(response => {
				log.info(`${LOG_FILE_NAME}publish2Datasource(sdkDatasourceWrapper) SUCCESS: ${JSON.stringify(response.data)}`);
				return;
			})
			.catch(err => {
				log.error(`${LOG_FILE_NAME}publish2Datasource(sdkDatasourceWrapper) FAILURE: ${JSON.stringify(err.response.data)}`);
				return;
			});
	}
}

module.exports.publishTrip = async function (data) {

	log.info(LOG_FILE_NAME + "PUBLISH_ETA_TRIP: to ServiceId=" + SRV_ETAService_Trip);
	var result = await sdkRequestWrapper.publish(SRV_ETAService_Trip, null, data)
		.then(response => {
			log.info(`${LOG_FILE_NAME}publishTrip(sdkRequestWrapper) SUCCESS: ${JSON.stringify(response.data)}`);
			return null;
		})
		.catch(err => {
			log.error(`${LOG_FILE_NAME}publishTrip(sdkRequestWrapper) FAILURE: ${JSON.stringify(err.response.data)}`);
			return null;
		});
	return result;
}

module.exports.subscribe = async function (data, type) {

	log.info(LOG_FILE_NAME + "PUBLISH_ETA_SUBSCRIPTION: to ServiceId=" + SRV_ETAService_Trip);
	//ToDo: data.data.subscription.subscriptionType = type.subscriptionType;
	await sdkRequestWrapper.publish(SRV_ETAService_Trip, null, data)
		.then(response => {
			log.info(`${LOG_FILE_NAME}subscribe(sdkRequestWrapper) SUCCESS: ${JSON.stringify(response.data)}`);
			return;
		})
		.catch(err => {
			log.error(`${LOG_FILE_NAME}subscribe(sdkRequestWrapper) FAILURE: ${JSON.stringify(err.response.data)}`);
			return;
		});
}

module.exports.position = async function (data) {

	log.info(LOG_FILE_NAME + "PUBLISH_POSITION: to ServiceId=" + SRV_ETAService_Trip);
	await sdkRequestWrapper.publish(SRV_ETAService_Trip, null, data)
		.then(response => {
			log.info(`${LOG_FILE_NAME}position(sdkRequestWrapper) SUCCESS: ${JSON.stringify(response.data)}`);
			return;
		})
		.catch(err => {
			log.error(`${LOG_FILE_NAME}position(sdkRequestWrapper) FAILURE: ${JSON.stringify(err.response.data)}`);
			return;
		});
}

module.exports.order = async function (data) {

	log.info(LOG_FILE_NAME + "PUBLISH_ORDER: to ServiceId=" + SRV_ETAService_Trip);
	await sdkRequestWrapper.publish(SRV_ETAService_Trip, null, data)
		.then(response => {
			log.info(`${LOG_FILE_NAME}order(sdkRequestWrapper) SUCCESS: ${JSON.stringify(response.data)}`);
			return;
		})
		.catch(err => {
			log.error(`${LOG_FILE_NAME}order(sdkRequestWrapper) FAILURE: ${JSON.stringify(err.response.data)}`);
			return;
		});
}
