/**
Author: Jürgen Stolz / PTV Group
Copyright: 2019
Project: Aeolix

Description:

History:
20190102 | Created

*/

const _ = require('lodash');

var logger = require('../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./MyAeolixSocket.js => ";

class MyAeolixSocket {


	/**
	 * ToDo Parameter aufräumen
	 * 
	 * @param {Datasource, ServiceRequest, ServiceResponse} type 
	 * @param {the unique MyAeolix id} typeID 
	 * @param {the "calling" SDK instance} sdk  => ToDo: wrong place ugly here
	 */
	constructor(type, typeID, socketControlCallback, socketDataCallback) {

		this.SERVER_URL = 'https://connect.myaeolix.eu';
		this.SOCKET_SERVER_URL = this.SERVER_URL + ":5002";
		this.REST_SERVER_URL = this.SERVER_URL + ":3456";

		this.type = null;
		this.typeID = null;
		this.socketMsgType = null;
		this.socketEmitType = null;
		this.socketUnsubscribedType = null;
		this.publishURL = null;
		this.setType(type, typeID);

		this.socket = null;

		/**
	  * This parameter is used to signal, that we (as middleware) want to unsubscribe from 
		* a Datasource, ServiceRequest or ServiceResponse
	  */
		this.forcedUnsubscribe = false;

		/**
		 * Used for the reconnect tries
		 */
		this.wait = ms => new Promise(resolve => setTimeout(resolve, ms));

		/**
		 * 
		 */
		this.socketControlCallback = socketControlCallback;
		this.socketDataCallback = socketDataCallback;
	}

	setToken(token) {

		this.token = token;
	}

	setKeyPair(keyPair) {

		this.ownPublicRSAKey = keyPair.public;
		this.ownPrivateRSAKey = keyPair.private;
	}

	setType(type, typeID) {

		this.typeID = typeID;

		this.type = type;
		if (type === "Datasource") {

			this.socketMsgType = "message-";
			this.socketEmitType = "subscribeDatasource";
			this.socketUnsubscribedType = "unsubscribedDataSource";
			this.publishURL = this.REST_SERVER_URL + '/sdkAPI/publish/' + typeID;
		}
		else if (type === "ServiceRequest") {

			this.socketMsgType = "serviceRequest-";
			this.socketEmitType = "subscribeServiceRequest";
			this.socketUnsubscribedType = "unsubscribedServiceRequest";
			this.publishURL = this.REST_SERVER_URL + '/sdkAPI/services/' + typeID + '/requests';
		}
		else if (type === "ServiceResponse") {

			this.socketMsgType = "serviceResponse-";
			this.socketEmitType = "subscribeServiceResponse";
			this.socketUnsubscribedType = "unsubscribedServiceResponse";
			this.publishURL = this.REST_SERVER_URL + '/sdkAPI/services/' + typeID + '/responses';
		}
	}

	deCipherTheData(data) {

		try {

			var encryptedAESKey = data.key;
			var buf = Buffer.from(encryptedAESKey, 'hex');

			var decryptedKey = crypto.privateDecrypt(this.ownPrivateRSAKey, buf);
			var AESKey = decryptedKey.toString('utf8');

			var iv = Buffer.from([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
			var decipher = crypto.createDecipheriv('aes-256-cbc', AESKey, iv);

			var decryptData = decipher.update(data.data, 'hex', 'utf8');
			decryptData += decipher.final('utf8');

			return { error: false, "decryptData": decryptData };
			//return decryptData;
		}
		catch (err) {

			//return (JSON.stringify(err));
			log.error(LOG_FILE_NAME + `deCipherTheData(..): SDK decipher data error=${err}`);
			return { error: true };
		}
	}

	/**
 * ToDo: Ugly move the code into the sdk-wrapper.js to hold the controls in the creating class
 * @param {*} sdkCtrlData 
 */
	control_callback(sdkCtrlData) {

		log.info(LOG_FILE_NAME + `control_callback(${this.type}) called: TypeID=${this.typeID}, msg=${JSON.stringify(sdkCtrlData)}`);

		if (sdkCtrlData.code === 50) {
			// nothing to do.
		}
		else if (sdkCtrlData.code === 200) {

			if (sdkCtrlData.msg != undefined && sdkCtrlData.msg != null) {

				if (sdkCtrlData.msg === "Unsubscribed.") {

					this.socket = null;
					if (this.forcedUnsubscribe === false) {

						this.subscribe();
					}
				}
				else {

				}
			}
		}
		else if (sdkCtrlData.code === 201) {

			if (sdkCtrlData.msg != undefined && sdkCtrlData.msg != null) {

				if (sdkCtrlData.msg == "Unsubscribed.") {

					if (!this.forcedUnsubscribe) {

						this.subscribe();
					}
				}
			}
		}
		else if (sdkCtrlData.code === 250) {

			// nothing to do.
		}
		else if (sdkCtrlData.code === 401) {

			/**
			 * ToDo: was soll ich tun?
			 * Das ist einer der Fehler die auftreten
			 * msg={"url":"https://aeon.atosresearch.eu:8080/auth/realms/aeolix/protocol/openid-connect/userinfo","status":401,"statusText":"Unauthorized","headers":{"_headers":{"connection":["close"],"content-type":["application/json"],"content-length":["82"],"date":["Fri, 07 Dec 2018 06:17:22 GMT"]}},"ok":false,"body":{"_readableState":{"objectMode":false,"highWaterMark":16384,"buffer":{"head":{"data":{"type":"Buffer","data":[123,34,101,114,114,111,114,34,58,34,105,110,118,97,108,105,100,95,116,111,107,101,110,34,44,34,101,114,114,111,114,95,100,101,115,99,114,105,112,116,105,111,110,34,58,34,84,111,107,101,110,32,105,110,118,97,108,105,100,58,32,84,111,107,101,110,32,105,115,32,110,111,116,32,97,99,116,105,118,101,34,125]},"next":null},"tail":{"data":{"type":"Buffer","data":[123,34,101,114,114,111,114,34,58,34,105,110,118,97,108,105,100,95,116,111,107,101,110,34,44,34,101,114,114,111,114,95,100,101,115,99,114,105,112,116,105,111,110,34,58,34,84,111,107,101,110,32,105,110,118,97,108,105,100,58,32,84,111,107,101,110,32,105,115,32,110,111,116,32,97,99,116,105,118,101,34,125]},"next":null},"length":1},"length":82,"pipes":null,"pipesCount":0,"flowing":null,"ended":true,"endEmitted":false,"reading":false,"sync":false,"needReadable":false,"emittedReadable":true,"readableListening":false,"resumeScheduled":false,"destroyed":false,"defaultEncoding":"utf8","awaitDrain":0,"readingMore":false,"decoder":null,"encoding":null},"readable":true,"domain":null,"_events":{},"_eventsCount":2,"_writableState":{"objectMode":false,"highWaterMark":16384,"finalCalled":false,"needDrain":false,"ending":true,"ended":true,"finished":true,"destroyed":false,"decodeStrings":true,"defaultEncoding":"utf8","length":0,"writing":false,"corked":0,"sync":false,"bufferProcessing":false,"writecb":null,"writelen":0,"bufferedRequest":null,"lastBufferedRequest":null,"pendingcb":0,"prefinished":true,"errorEmitted":false,"bufferedRequestCount":0,"corkedRequestsFree":{"next":null,"entry":null}},"writable":false,"allowHalfOpen":true,"_transformState":{"needTransform":false,"transforming":false,"writecb":null,"writechunk":null,"writeencoding":"buffer"},"_writev":null},"bodyUsed":false,"size":0,"timeout":0,"_raw":[],"_abort":false}
			 * 
			 */
			if (!this.forcedUnsubscribe) {

				this.subscribe();
			}

			log.warn(LOG_FILE_NAME + `control_callback(${this.type}): TypeID=${this.typeID}, msg=${JSON.stringify(sdkCtrlData)}, Check socket subscription!`);
		}
		else if (sdkCtrlData.code === 410) {

			/**
			 * ToDo: was soll ich tun?
			 * Das ist einer der Fehler die auftreten
			 * 
			 */
			if (!this.forcedUnsubscribe) {

				this.subscribe();
			}

			log.warn(LOG_FILE_NAME + `control_callback(${this.type}): TypeID=${this.typeID}, msg=${JSON.stringify(sdkCtrlData)}, Check socket subscription!`);
		}
		else {

			log.error(LOG_FILE_NAME + `control_callback(${this.type}): TypeID=${this.typeID}, msg=${JSON.stringify(sdkCtrlData)}, Control msg not handled.`);
		}
	}


	/**
	 * Wenn ich es richtig gesehen sind alle 3 Methoden im SDK fast identisch, das kann man
	 * doch vereinfachen. => getan :)
	 * 
	 * ToDo: this.token überprüfen ob man das von außen noch aktualisieren muss. Vielleicht kommt dann der
	 * Fehler access Token o.s.ä. nicht mehr.
	 * 
	 * Der Type wird im Moment im Constructor übergeben
	 */
	subscribe() {

		log.info(LOG_FILE_NAME + `subscribe(${this.type}) called: TypeID=${this.typeID}`);
		this.socket = io.connect(this.SOCKET_SERVER_URL, { "query": { "token": this.token }, "reconnection": false });

		var me = this;
		this.socket.on('control', this.control_callback.bind(this));
		//this.socket.on('control', this.socketControlCallback.bind(this));
		/* 		this.socket.on('control', (data) => {
		
					log.info(LOG_FILE_NAME + `subscribe(${this.type}) => socket.control called: TypeID=${this.typeID}, data=${data}`);
					this.socketControlCallback(this.type, data);
				});
		 */

		this.socket.on('connected', (data) => {

			this.socket.on(me.socketMsgType + me.typeID, function manageDataMessages(data) {

				var responseData;
				if (data.key) {

					responseData = me.deCipherTheData(data);
				} else {

					responseData = data;
				}
				//ToDo: testen me.data_callback(responseData.decryptData);
				var jsonData = JSON.parse(responseData.decryptData)
				var userID = jsonData.userID;
				var payload = jsonData.data;
				var payloadObject;
				if (payload != null) {
					payloadObject = JSON.parse(payload);
				}
				else {
					payloadObject = jsonData;
				}
				var extendedData = {

					error: responseData.error,
					userID: userID,
					data: payloadObject,
					type: me.type,
					typeID: me.typeID
				}
				/* 				jsonData = JSON.parse(receivedData)
								var userID = jsonData.userID;
								var payload = jsonData.data;
								var payloadObject = JSON.parse(payload);
				 */
				me.socketDataCallback(extendedData);
			})

			var subscriptionData = {

				userToken: this.token,
				key: this.ownPublicRSAKey
			};
			if (this.type === "Datasource") {

				subscriptionData["datasourceID"] = this.typeID;
			}
			else {

				subscriptionData["serviceID"] = this.typeID;
			}

			this.socket.emit(this.socketEmitType, subscriptionData);
		});

		this.socket.on(this.socketUnsubscribedType, (reason) => {

			log.info(LOG_FILE_NAME + `${this.socketUnsubscribedType}(${this.type}) => socket.unsubscribed called: TypeID=${this.typeID}, reason=${reason}`);
			unsubscribeFlag = true;

			//next(response);
			var sdkCtrlData = {

				code: 201,
				msg: "Unsubscribed.",
				reason: reason
			}
			this.control_callback(sdkCtrlData);

		});

		this.socket.on('disconnect', (reason) => {

			log.fatal(LOG_FILE_NAME + `subscribe(${this.type}) => socket.disconnect called: TypeID=${this.typeID}, reason=${reason}`);
			this.socket.close();
			this.socket = null;

			/**
			 * Info weiterleiten, dass wir uns neu subscriben müssen, das SDK hat uns abgemeldet
			 * 
			 * ToDo: wir müssen auf die reason achten, es kann auch
			 * - "io server disconnect" sein und dann müssen wir den Socket wieder neu "machen"
			 * 
			 * ToDo: Wir müssen den "error" code sinnvoller setzen
			 */
			var sdkCtrlData = {

				error: true,
				code: 410,
				msg: reason,
				reason: reason
			}
/* 
aus dem SDK von Luis Vers. 0.3.2
			var disconnectMessage = {
				error: true,
				code: 410,
				msg:reason
			}
	
 */			this.control_callback(sdkCtrlData);

			/**
			 * ToDo: Test wohin weiterleiten
			 */
			//this.socketControlCallback(this.typeID, reason);
		});
	}
}

module.exports = MyAeolixSocket;