/**
Author: Jürgen Stolz / PTV Group
Copyright: 2019
Project: Aeolix

Description:

History:
20190102 | Created

*/

const _ = require('lodash');

var logger = require('../../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./MyAeolixHttp.js => ";

//import axios from 'axios'; //Typescript?
var axios = require('axios');
var crypto = require('crypto');
var keypair = require('keypair');

class MyAeolixHttp {


	constructor() {

		this.SERVER_URL = 'https://connect.myaeolix.eu';
		this.REST_SERVER_URL = this.SERVER_URL + ":3456";

		this.rsaPublicKey = '-----BEGIN PUBLIC KEY-----\n\
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAszaKYEpbqzoR3VtQIZ3h\n\
eTE6WpkcQXEasKWxkMr2MVzx8OFPz5j6918Y9s28digReLizkdOFnrbs0LctpBwA\n\
7tHKNt7/1cpQQ6dJDhA8k624QtBA2CRXIh6zLVfMdfqbFHW5iwxJgDz2vpdbRd7L\n\
1uIvsrzVHU/6lsqDw3ndQayPYMCSIqq3TeurmZ2zO2PR/D3hr2V5HrNRl6687shI\n\
GB6QM0huBIfYpZIEbPmnZD5Q4EnEI/Fzx9AseYmvgHfNvPm0P1aYS/GDiYPXSBxC\n\
p0tMuJVdd7G2RDiFM86/EV7DuBpOWLU75SsY8mi+rk4eiins+B3Kdwfaq6oBRlhJ\n\
GQIDAQAB\n\
-----END PUBLIC KEY-----';

		this.token = "undefined";
		this.refreshToken = "undefind";

		this.keyPair = null;
		this.generateRandomRSAKeys();

		this.availableSocketSubscriptions = new Map();
		this.forceReInitialize = false;
	}

	generateRandomRSAKeys() {

		log.info(LOG_FILE_NAME + "generateRandomRSAKeys(..): called");

		this.keyPair = keypair();
	}

	/**
	 * @param {*} userData 
	 */
	async login2Aeolix(userData) {

		if (log.isDebugEnabled) {

			log.debug(LOG_FILE_NAME + "login2Aeolix(..): " + JSON.stringify(userData));
		}
		return await axios.post(this.REST_SERVER_URL + '/sdkAPI/login', userData);
	}

	/**
	 * Updates the token in the SDKWrapper and in the SDKSUbscriptionWrapper
	 * @param {*} access_token 
	 * @param {*} refresh_token 
	 */
	setTokens(access_token, refresh_token) {

		this.token = access_token;
		this.refreshToken = refresh_token;

		this.availableSocketSubscriptions.forEach(function (sdkSubscriptionWrapper) {

			sdkSubscriptionWrapper.setToken(this.token);
		}.bind(this));
	}

	async refreshAuthToken() {

		if (this.refreshToken === null || this.refreshToken === undefined) {

			/**
			 * ToDo: Inform the application administration.
			 * e-Mail, log analytics, ....
			 */
			log.fatal(LOG_FILE_NAME + "refreshAuthToken(..): Refresh token not found. Administration needed");
			return;
		}

		var body = { "refreshToken": this.refreshToken };
		var authOptions = {
			method: 'POST',
			url: this.REST_SERVER_URL + '/sdkAPI/refresh',
			data: body,
			headers: {
				'Authorization': "Bearer " + this.token,
				'Content-Type': 'application/json'
			},
			json: true
		};
		return await axios(authOptions);
	}

	async logout() {

		log.info(LOG_FILE_NAME + `logout(..):`);
		if ((this.token === null || this.token === undefined) || (this.refreshToken === null || this.refreshToken === undefined)) {

			token = null;
			refreshToken = null;
			/**
			 * ToDo: was machen wir damit
			 */
			//next(LOGOUT_OK); 
			log.error(LOG_FILE_NAME + `logout(..): Internal parameter failures, token or refreshToken not valid`);
			return;
		}

		var body = { "refresh_token": this.refreshToken };
		var authOptions = {
			method: 'POST',
			url: this.REST_SERVER_URL + '/sdkAPI/logout',
			data: body,
			headers: {
				'Authorization': "Bearer " + this.token,
				'Content-Type': 'application/json'
			},
			json: true
		};
		return await axios(authOptions);
	}

	/**
	 * 
	 * @param {*} newToken 
	 */
	async setToken(newToken) {

		log.info(LOG_FILE_NAME + `setToken(..): ${newToken}`);
		if (newToken === null || newToken === undefined) {

			/**
			 * ToDO: Was soll ich tun?
			 */
			log.error(LOG_FILE_NAME + `setToken(..): failed. Parameter newToken is null or undefined. Administration needed`);
			return;
		}

		var body = { "token": newToken };
		var authOptions = {
			method: 'POST',
			url: this.REST_SERVER_URL + '/sdkAPI/exchangeToken',
			data: body,
			headers: {
				'Authorization': "Bearer " + this.token,
				'Content-Type': 'application/json'
			},
			json: true
		};
		return await axios(authOptions);
	}

	generateRandomAESKey() {

		var key = crypto.randomBytes(16);
		return key.toString('hex');
	}

	cipherTheData(data, userID) {

		var requestData;
		var requestKey;
		var dataAsString = JSON.stringify(data);
		try {
			var AESKey = this.generateRandomAESKey();
			var iv = Buffer.from([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);

			var cipher = crypto.createCipheriv('aes-256-cbc', AESKey, iv);

			requestData = cipher.update(dataAsString, 'utf8', 'hex');
			requestData += cipher.final('hex');

			var buf = Buffer.from(AESKey, 'utf8');

			var encryptedKey = crypto.publicEncrypt(this.rsaPublicKey, buf);
			requestKey = encryptedKey.toString('hex');

			var content = {
				"data": requestData,
				"key": requestKey
			}
			if (userID != null && userID != undefined) {

				content["user"] = userID;
			}
			return { error: false, data: content };
		} catch (err) {

			log.error(LOG_FILE_NAME + `cipherTheData(..): SDK cipher data error=${err}`);
			return { error: true };
		}
	}

	/**
	 * ToDo: We need a useful return in case of an error => this.token === null.
	 * 
	 * ToDo: UserId besser behandeln/implementieren
	 * 
	 * 
	 * @param {*} typeID 
	 * @param {*} userID 
	 * @param {*} data2Publish 
	 */
	async publish(typeID, userID, data2Publish) {

		log.info(LOG_FILE_NAME + `publish(${typeID}) called`);
		if (this.token === null || this.token === undefined) {

			/**
			 * ToDo: was soll ich tun?
			 * 
			 */
			log.fatal(LOG_FILE_NAME + `publish(${typeID}): internal parameter failure (token not valid). Administration needed`);
			//next(UNAUTHORIZED);
			return;
		}

		var sdkSubscriptionWrapper = this.availableSocketSubscriptions.get(typeID);
		if (sdkSubscriptionWrapper === null || sdkSubscriptionWrapper === undefined) {

			log.fatal(LOG_FILE_NAME + `publish(${typeID}): failure, typeID not valid. Administration needed`);
			return;
		}

		var requestBody = this.cipherTheData(data2Publish, userID);
		if (requestBody.error === true) {

			log.error(LOG_FILE_NAME + `publish(${typeID}): SDK cipher data error.`);
			//next(CIPHER_ERROR);
			return;
		}
		var authOptions = {
			
			method: 'POST',
			url: sdkSubscriptionWrapper.publishURL,
			data: requestBody.data,
			headers: {
				'Authorization': "Bearer " + this.token,
				'Content-Type': 'application/json'
			},
			json: true
		};

		if (userID != null && userID != undefined) {

			authOptions.url = `https://connect.myaeolix.eu:3456/sdkAPI/services/${typeID}/responses`;
			//authOptions.data["user"] = userID;
		}
		if (log.isDebugEnabled) {

			log.debug(LOG_FILE_NAME + `publish(${typeID}): authOptions=${JSON.stringify(authOptions)}.`);
		}

		return await axios(authOptions);
	}

	/**
	 * ToDo: We need a useful return i case of an error or informational return => this.availableSocketSubscriptions.....
	 * @param {*} type 
	 * @param {*} typeID 
	 * @param {*} socketDataCallback 
	 */
	subscribe(type, typeID, socketCtrlCallback, socketDataCallback) {

		log.info(LOG_FILE_NAME + `subscribe(${type}): TypeID=${typeID}`);
		if (this.availableSocketSubscriptions.has(typeID)) {

			log.error(LOG_FILE_NAME + `subscribe(${type}): TypeID=${typeID} instance exists`);
			return false;
		}
		/**
		 * Die Subscriptions noch in einer Map speichern, damit wir wieder darauf
		 * zugreifen können.
		 */
		var sdkSubscriptionWrapper = new SDKSubscriptionWrapper(type, typeID, socketCtrlCallback, socketDataCallback);
		sdkSubscriptionWrapper.setToken(this.token);
		sdkSubscriptionWrapper.setKeyPair(this.keyPair);
		sdkSubscriptionWrapper.subscribe();
		this.availableSocketSubscriptions.set(typeID, sdkSubscriptionWrapper);
	}

}

module.exports = MyAeolixHttp;