/**
Author: Jürgen Stolz / PTV Group
Copyright: 2019
Project: Aeolix

Description:

History:
20190102 | Created

*/
const _ = require('lodash');

var logger = require('../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./MyAeolix.js => ";

var MyAeolixHttp = require('./MyAeolixWrapper/MyAeolixHttp');

class MyAeolix {

	//dbMap = new Map(); // ToDo: testen ob das vorher anlegen geht
	constructor() {

		this.initialize === false;

		this.loggedIn = false;
		this.myAeolixHttp = new MyAeolixHttp();

		this.subscriptionsMap = new Map();
		if (log.isDebugEnabled) {

			log.debug(LOG_FILE_NAME + `constructor(..) call finished`);
		}
	}

	enableSDK() {

		this.initialize = true;
	}

	disableSDK() {

		this.initialize = false;
		// ToDo: Logout aufrufen => falls angemeldet :)
	}

	async init() {

		if (this.initialize === false) {

			log.warn(`${LOG_FILE_NAME}init(..) is deactivated!!!`);
			return;
		}
		/**
		 * ToDo: das sollte aus einer Datenbank kommen
		 */
		var user = {
			"username": "juergen.stolz@ptvgroup.com",
			"password": "VoidhawkBeiAtos",
			"type": "user"
		};

		if (this.loggedIn === false) {

			await this.myAeolixHttp.login2Aeolix(user)
				.then(response => {

					log.info(`${LOG_FILE_NAME}login2Aeolix(MyAeolixHttp) SUCCESS: ${JSON.stringify(response.data)}`);
					this.myAeolixHttp.setTokens(response.data.access_token, response.data.refresh_token);
					this.loggedIn = true;
				})
				.catch(err => {

					log.error(`${LOG_FILE_NAME}login2Aeolix(MyAeolixHttp) FAILURE: ${err}`);
					this.loggedIn = false;
					return;
				});
		}

		/**
		* keepSDKAlive wird dazu verwendet alle sdks am Leben halten
		*/
		this.keepSDKAlive(1 * 60 * 1000);

		/**
		 * ToDo: wir müssen uns die gewünschten Sockets subscriben
		 */
	}

	async wait(timer) {

		if (log.isDebugEnabled) {

			log.debug(`${LOG_FILE_NAME}wait(MyAeolixHttp) entry.`);
		}
		return new Promise(resolve => setTimeout(resolve, timer))
	}

	async keepSDKAlive(timer) {

		while (this.initialize === true) {

			await this.wait(timer);
			if (this.loggedIn === false) {

				this.init();
				return;
			}

			try {

				//				await wait(timer);
				await this.myAeolixHttp.refreshAuthToken()
					.then(response => {

						log.info(`${LOG_FILE_NAME}refreshAuthToken(MyAeolixHttp) SUCCESS: ${JSON.stringify(response.data)}`);
						this.myAeolixHttp.setTokens(response.data.access_token, response.data.refresh_token);
					})
					.catch(err => {
						log.error(`${LOG_FILE_NAME}refreshAuthToken(MyAeolixHttp) FAILURE: ${JSON.stringify(err.response.data)}`);
						/**
						 * ToDo: Dieser Fehler tritt auf, was soll ich tun?
						 */
						this.loggedIn = false;
					});
			} catch (e) {

				/**
				 * ToDo: Egal wir machen weiter, eventuell am SDK ein Flag setzen, dass wir uns neu anmelden müssen
				 */
				log.fatal(`${LOG_FILE_NAME}refreshAuthToken(..) FAILURE: ${JSON.stringify(e)}`);
				this.loggedIn = false;
			}
		}
	}

	registerSubscription() {

		//this.subscriptionsMap.set(dbName, database);

		//		return database;
	}

	getSubscription(id) {

		//return _.get(this.subscriptionsMap.get(dbName), 'DBConnection');
	}
}

module.exports = new MyAeolix();