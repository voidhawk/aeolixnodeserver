var express = require('express');
var router = express.Router();

var logger = require('../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./xServerRoutes.js => ";
var databaseHandler = require('../db/DatabaseHandler');

router.get('/query', (req, res, next) => {

	if (log.isDebugEnabled) {

		log.debug(LOG_FILE_NAME + "GET => URL(/query) called");
		log.debug(LOG_FILE_NAME + "QueryParameters_ " + JSON.stringify(req.query));
		log.debug(LOG_FILE_NAME + "QueryParameter_query=" + req.query.finder);
		log.debug(LOG_FILE_NAME + "QueryParameter_projection=" + req.query.projection);
	}

	var connection = databaseHandler.getDatabaseConnection("mLab");
	var xServerResponseModel = connection.model("XServerResponseModel");

	var jsonFinderObject = JSON.parse(req.query.finder);
	var jsonProjectionObject = JSON.parse(req.query.projection);

	xServerResponseModel.find(jsonFinderObject, jsonProjectionObject)
		.then(documents => {

			console.log("Document count: " + documents.length);
			return res.status(200).json({ message: 'GET: SUCCESS', documents: documents });
		})
	//	res.status(204).json({ message: 'GET: SUCCESS_NO_CONTENT' });
});

/**
 * Abfragen von bestimmten xServer Inhalten
 * db.getCollection('xserverresponses').find({'name':/F_100-HH-BR/}, {'name': 1, 'routeStartTime': 1, 'response.distance': 1, 'response.travelTime': 1})
 * 
 * db.getCollection('xserverresponses').find({'name':/AMS/, 'routeStartTime':{ $gte: ISODate("2018-07-19T00:00:00.000Z"), $lt: ISODate("2018-07-19T23:59:59.000Z") } }, {'name': 1, 'routeStartTime': 1, 'response.distance': 1, 'response.travelTime': 1} )
 */
router.get('/', function (req, res, next) {

	console.log("GET: api/V1/xServer called");
	console.log("QueryParameters_ " + JSON.stringify(req.query));
	console.log("QueryParameter_type=" + req.query.type);
	console.log("QueryParameter_id=" + req.query.id);

	var connection = databaseHandler.getDatabaseConnection("mLab");
	var xServerResponseModel = connection.model("XServerResponseModel");

	var finder = { type: req.query.type, "name": { $regex: '.*' + req.query.id + '.*' }, 'routeStartTime': { $gte: req.query.from, $lt: req.query.until } };
	var jsonFinderObject = JSON.parse(JSON.stringify(finder));
	var projection = { 'name': 1, 'routeStartTime': 1, 'response.distance': 1, 'response.travelTime': 1 };
	var jsonProjectionObject = JSON.parse(JSON.stringify(projection));

	//xServerResponseModel.findOne()
	xServerResponseModel.find(jsonFinderObject, jsonProjectionObject)
		.then(documents => {

			console.log("Anzahl Dokumente: " + documents.length);
			return res.status(200).json({ message: 'GET: SUCCESS', documents: documents });
		});
	//res.status(204).json({ message: 'GET: SUCCESS_NO_CONTENT' });
	//return res.status(200).json({ message: 'GET: SUCCESS_NO_CONTENT' });
});

/**
 * Diese Methode wird benutzt um xServer Inhalte abzuspeichern
 */
router.post('/', function (req, res, next) {

	console.log("POST: api/V1/xServer called");
	const value = req.body;
	//console.log("api/V1/xServer body: " + JSON.stringify(value));
	/**
	 * Do something
	 */
	var connection = databaseHandler.getDatabaseConnection("mLab");
	var xServerResponseModel = connection.model("XServerResponseModel");

	xServerResponseModel.create({
		"name": req.body.name,
		"type": req.body.type,
		"routeStartTime": req.body.routeStartTime,
		"request": req.body.request,
		"response": req.body.response
	}).then((savedObject) => {
		console.log(savedObject.name + ": " + savedObject);
	}).catch(() => {
		console.log("research-ptv: MongoDB create savedObject FAILURE");
		res.status(400).json({ message: 'Create and save in DB failed' });
		return;
	});

	/* 	
		const xServerResponse = new XServerResponse({
	
			name: req.body.name,
			type: req.body.type,
			routeStartTime: req.body.routeStartTime,
			request: req.body.request,
			response: req.body.response
		});
	
		xServerResponse.save();
	 */
	res.status(201).json({ message: 'POST: store xServerResponse was successfull' });
});

module.exports = router;
