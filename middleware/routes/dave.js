var express = require('express');
var router = express.Router();

var logger = require('../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./dave.js => ";

router.get('/login', function (req, res, next) {

	log.info(LOG_FILE_NAME + "GET => URL(/login) called");
	return res.status(200).json({ info: 'login' });
});

module.exports = router;