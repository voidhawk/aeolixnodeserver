var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();

var logger = require('../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./auth.js => ";

//const _ = require('lodash');
const UIDGenerator = require('uid-generator');
const uidgen = new UIDGenerator(); // Default is a 128-bit UID encoded in base58

var TheVoid = require('thevoidmodelmodule/dist/shared/models/thevoid-user.model');

var databaseHandler = require('../db/DatabaseHandler');

router.post('/user', function (req, res, next) {

	log.info(LOG_FILE_NAME + "POST => URL(/user) called");
	var connection = databaseHandler.getDatabaseConnection("mLabAccount");
	var userModel = connection.model("UserModel");
	userModel.create({
		"firstName": req.body.firstName,
		"lastName": req.body.lastName,
		"sign": req.body.sign,
		"email": req.body.email,
		"pwd": req.body.pwd,
		"tenantId": req.body.tenantId,
		"type": req.body.type
	}).then((savedObject) => {
		log.info(LOG_FILE_NAME + `Create and save User.Lastname(${req.body.lastName}). User=${JSON.stringify(savedObject)}`);
		return res.status(200).json({

			error: false,
			message: "User created",
			user: savedObject
		}
		);
	}).catch((err) => {

		log.error(LOG_FILE_NAME + `Create and save Tenant.Name(${req.body.name}) in DB failed. Error=${JSON.stringify(err)}`);
		return res.status(400).json({ error: true, message: `Create and save Tenant.Name(${req.body.name}) in DB failed. Error=${err}` });
	});
});

/**
 * ./api/V1/auth/login
 * Lokal
 * http://localhost:50000/api/V1/auth/login
 * Produktiv
 * https://..../api/V1/auth/login
 */
router.post('/login', function (req, res, next) {

	log.info(LOG_FILE_NAME + "POST => URL(./api/V1/auth/login) called");
	const email = req.body.email;
	const pwd = req.body.pwd;
	const type = req.body.type;
	const sessionId = req.body.userSessionId;

	var userFinder = { "email": email, "pwd": pwd, "type": type };

	var connection = databaseHandler.getDatabaseConnection("mLabAccount");
	var userModel = connection.model("UserModel");
	userModel.findOne(userFinder)
		.then(document => {

			if (document === null) {

				log.error(LOG_FILE_NAME + `POST => URL(./api/V1/auth/login) User or password wrong: ${JSON.stringify(userFinder)}`);
				return res.status(400).json({

					error: true,
					message: "User or password wrong"
				});
			}
			log.info(LOG_FILE_NAME + "POST => URL(./api/V1/auth/login) returned document: " + JSON.stringify(document));
			const tenantId = document.tenantId;//"Pqb59eLGtf9yYDhXa73xuv";//findUserIdForEmail(email);
			const jwtBearerToken = jwt.sign({ tenant: tenantId }, "TheHaulierSecret", {
				algorithm: 'HS256',
				expiresIn: 120
			});
			return res.status(200).json({

				error: false,
				message: "User loggedIn",
				token: jwtBearerToken,
				expiresIn: 120,
				userConfig: document
			});

		}).catch(err => {
			log.error(LOG_FILE_NAME + `POST => URL(./api/V1/auth/login): ${JSON.stringify(err)}`);
			return res.status(400).json({

				error: true,
				message: "User or password wrong"
			});
		});
});

router.post('/tenant', function (req, res, next) {

	log.info(LOG_FILE_NAME + "POST => URL(/tenant) called");

	var tenantId = uidgen.generateSync();
	var connection = databaseHandler.getDatabaseConnection("mLabAccount");
	var tenantModel = connection.model("TenantModel");
	tenantModel.create({
		"name": req.body.name,
		"description": req.body.description,
		"tenantId": tenantId
	}).then((savedObject) => {

		log.info(LOG_FILE_NAME + `Create and save Tenant.Name(${req.body.name}). Tenant=${JSON.stringify(savedObject)}`);
		return res.status(200).json({

			error: false,
			message: "Tenant created",
			tenant: savedObject
		}
		);
	}).catch((err) => {

		log.error(LOG_FILE_NAME + `Create and save Tenant.Name(${req.body.name}) in DB failed. Error=${JSON.stringify(err)}`);
		return res.status(400).json({ error: true, message: `Create and save Tenant.Name(${req.body.name}) in DB failed. Error=${err}` });
	});
});

/**
 * /api/V1/auth/tenant/Pqb59eLGtf9yYDhXa73xuv
 */
router.put('/tenant/:id', async function (req, res, next) {

	log.info(LOG_FILE_NAME + `POST => URL(/tenant/id=${req.params.id}) called`);

	var connection = databaseHandler.getDatabaseConnection("mLabAccount");
	var tenantModel = connection.model("TenantModel");
	var tempRight = {

		name: req.body.name
	};

	await tenantModel.findOneAndUpdate(
		{ 'tenantId': req.params.id },
		{
			'updateTime': new Date(),
			$addToSet: { 'rights': tempRight }
		},
		{ new: true }
	).then((updatedObject) => {

		return res.status(200).json(
			{
				error: false,
				message: `Tenant updated SUCCESS`,
				tenant: updatedObject
			});
	}).catch((err) => {

		return res.status(400).json({ error: true, message: `Tenant updated FAILURE` });
	});;


	/* 	tenantModel.create({
			"name": req.body.name,
			"description": req.body.description,
			"tenantId": tenantId
		}).then((savedObject) => {
	
			log.info(LOG_FILE_NAME + `Create and save Tenant.Name(${req.body.name}). Tenant=${JSON.stringify(savedObject)}`);
			return res.status(200).json({
	
					info: "Tenant created",
					tenant: savedObject
				}
			);
		}).catch((err) => {
	
			log.error(LOG_FILE_NAME + `Create and save Tenant.Name(${req.body.name}) in DB failed. Error=${JSON.stringify(err)}`);
			return res.status(400).json({ message: `Create and save Tenant.Name(${req.body.name}) in DB failed. Error=${err}` });
		});
	 */
});

router.get('/tenant', async function (req, res, next) {

	log.info(LOG_FILE_NAME + "GET => URL(/tenant) called");
	var connection = databaseHandler.getDatabaseConnection("mLabAccount");
	var tenantModel = connection.model("TenantModel");

	await tenantModel.find()
		.then(documents => {

			return res.status(200).json({ error: false, message: 'GET: /tenant/:id SUCCESS', tenants: documents });
		}).catch((err) => {

			return res.status(400).json({ error: true, message: `GET: /tenant/:id FAILURE Error=${err}` });
		});
});

router.get('/tenant/:id', async function (req, res, next) {

	log.info(LOG_FILE_NAME + `GET => URL(/tenant/:id=${req.params.id}) called`);

	var connection = databaseHandler.getDatabaseConnection("mLabAccount");
	var tenantModel = connection.model("TenantModel");

	var finder = { tenantId: req.params.id };
	var jsonFinderObject = JSON.parse(JSON.stringify(finder));
	/* 	var projection = { 'name': 1, 'routeStartTime': 1, 'response.distance': 1, 'response.travelTime': 1 };
		var jsonProjectionObject = JSON.parse(JSON.stringify(projection));
	 */
	await tenantModel.findOne(jsonFinderObject)
		.then(document => {

			return res.status(200).json({ error: false, message: 'GET: /tenant/:id SUCCESS', tenant: document });
		}).catch((err) => {

			return res.status(400).json({ error: true, message: `GET: /tenant/:id FAILURE Error=${err}` });
		});
});

//*****************************************************************//
// Ab hier beginnt alles was mit Right(s) zu tun
/**
 * Add a new RIGHT
 */
router.post('/right', function (req, res, next) {

	log.info(LOG_FILE_NAME + "POST => URL(/right) called");

	var connection = databaseHandler.getDatabaseConnection("mLabAccount");
	var rightModel = connection.model("RightModel");
	rightModel.create({
		"name": req.body.name,
		"description": req.body.description,
	}).then((savedObject) => {

		log.info(LOG_FILE_NAME + `Create and save Role.Name(${req.body.name}). Tenant=${JSON.stringify(savedObject)}`);
		return res.status(200).json({

			error: false,
			message: "Right created",
			right: savedObject
		}
		);
	}).catch((err) => {

		log.error(LOG_FILE_NAME + `Create and save Role.Name(${req.body.name}) in DB failed. Error=${JSON.stringify(err)}`);
		return res.status(400).json({ error: true, message: `Create and save Tenant.Name(${req.body.name}) in DB failed. Error=${err}` });
	});
});

/**
 * Query all available roles
 */
router.get('/right', async function (req, res, next) {

	log.info(LOG_FILE_NAME + "GET => URL(/right) called");
	var connection = databaseHandler.getDatabaseConnection("mLabAccount");
	var rightModel = connection.model("RightModel");

	await rightModel.find()
		.then(documents => {

			return res.status(200).json({ error: false, message: 'GET: /right/:id SUCCESS', roles: documents });
		}).catch((err) => {

			return res.status(400).json({ error: true, message: `GET: /right/:id FAILURE Error=${err}` });
		});
});

module.exports = router;
