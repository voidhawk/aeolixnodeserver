var express = require('express');
var router = express.Router();

var logger = require('../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./myaeolix.js => ";

router.get('/login', function (req, res, next) {

	log.info(LOG_FILE_NAME + "GET => URL(/login) called");
	return res.status(200).json({ info: 'login' });
});

/* app.post('/api/V1/aeolix/subscribe', (req, res, next) => {

	log.info(LOG_FILE_NAME + "POST => URL(./api/V1/aeolix/subscribe) called");
	var type = JSON.parse(req.query.subscriptionType);
	try {

		myAeolix.subscribe(req.body, type);
		return res.status(201).json({ message: 'POST=>/api/V1/aeolix/subscribe: Publish was successful' });
	}
	catch (e) {

		log.error(LOG_FILE_NAME + "POST=>/api/V1/aeolix/subscribe: Publish failed: " + e);
		return res.status(400).json({ message: 'POST=>/api/V1/aeolix/subscribe: Publish failed' });
	}
});

 */
module.exports = router;