var express = require('express');
var router = express.Router();

var logger = require('../logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./appRoutes.js => ";

router.get('/version', (req, res, next) => {

	log.info(LOG_FILE_NAME + "GET => URL(/version) called");

	var version = "3.0.0";

	return res.status(200).json({ version: version });
});

router.get('/keepAlive', function (req, res, next) {

	log.info(LOG_FILE_NAME + "GET => URL(/keepAlive) called");
	return res.status(200).json({ info: 'I am alive' });
});

module.exports = router;