const mongoose = require('mongoose');

const xServerResponseSchema = mongoose.Schema({

	name: String,
	type: String,
	creationTime: { type: Date, default: Date.now},
	routeStartTime: { type: Date},
	request: Object,
	response: Object
});

module.exports = mongoose.model('XServerResponse_old_version', xServerResponseSchema);