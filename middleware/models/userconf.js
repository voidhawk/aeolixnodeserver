const mongoose = require('mongoose');

const userSchema = mongoose.Schema({

	name: String,
	date: { type: Date, default: Date.now},
	comments: [{body: String, date: Date}],
	temp: {type: String, required: true}
});

module.exports = mongoose.model('UserConfig', userSchema);