/**
 * Atlas
 * MongoDB Anmeldung
 * mywwwuser@gmail.com
 * 1JuergenBeiAtlas#
 * 
 * 
 * MongoDB Atlas DB User
 * Voidhawk
 * gxpC8nPfeghwro6Q
 * 
 */
const express = require('express');

var appRoutes = require('./routes/appRoutes.js');

/**
 * Parser für req Inhalte
 * app.use(...) nicht vergessen!!!!
 */
const bodyParser = require('body-parser');

/**
 * Hier folgen die Mongoose Imports, Models, Schemas usw.
 * Das ganze ohne Extension
 */
var databaseHandler = require('./db/DatabaseHandler');

var env = require('dotenv').load();

/**
 * Logging functionality
 */
var logger = require('./logging/logger.js')
var log = logger.logger;
const LOG_FILE_NAME = "./app.js => ";

var DAveTourAPI = require('./dave/dave-tour.js');

var EmitterHandler = require('./notification/emitterHandler.js');
var PublishRequest = require('./notification/PublishRequest');

var appRoutes = require('./routes/appRoutes');
var authRoutes = require('./routes/auth');
var daveRoutes = require('./routes/dave');
var aeolixRoutes = require('./routes/myaeolix');
var xServerRoutes = require('./routes/xServerRoutes');
var theHaulierTour = require('./routes/TheHaulier/th-tour');


log.trace("");
log.trace("***** Node server started *****");

/* 
var researchOpts = {
	useNewUrlParser: true,
	auth: {
		user: 'research-ptv',
		password: 'dAF2l0NQ8qiCfwNDYS9wjmc4YBUuGRhRFfJhJWQv8LRksUNqedVB0MzUjTcOf7PUEfHUKF6cSOTXhAlbZQCTgg=='
	}
};
databaseHandler.registerDatabase("Data", "mongodb://research-ptv.documents.azure.com:10255/DAve?ssl=true", researchOpts, "data");
*/

var mLabsOpts = { useNewUrlParser: true };
databaseHandler.registerDatabase("mLab", "mongodb://aeolix:aeolix@ds139352.mlab.com:39352/aeolix", mLabsOpts, "data");
databaseHandler.registerDatabase("mLabAccount", "mongodb://aeolix:aeolix@ds139352.mlab.com:39352/aeolix", mLabsOpts, "accountmgmt");
// JSt: ToDo überprüfen ob mit dieser DB alles funktioniert
databaseHandler.registerDatabase("Data", "mongodb://aeolix:aeolix@ds139352.mlab.com:39352/aeolix", mLabsOpts, "data");

/**
 * Vorbereitung für Aeolix
 */
const myAeolix = require('./aeolix/aeolix.js');
myAeolix.setDatabase(databaseHandler.getDatabaseConnection("Data"));
myAeolix.init();

/**
 * Vorbereitung für MyAeolix SDK Ver. > 3.x.x
 */
/* 
const sdkMyAeolix = require('./aeolix/MyAeolix');
sdkMyAeolix.enableSDK();
sdkMyAeolix.init();
 */

const UserConfig = require('./models/userconf');

var connection = databaseHandler.getDatabaseConnection("Data");
var daveTourModel = connection.model("DAveTourModel");

const wait = ms => new Promise(resolve => setTimeout(resolve, ms))
const writeWhenExists = async entry => {

	let counter = 0;
	let exists = await daveTourModel.findOne({ 'tour.subscriptions.subscriptionID': entry.subscriptionID });
	while (!exists) {
		console.log(`Entry with id ${entry.subscriptionID} does not exist yet.`)
		exists = await daveTourModel.findOne({ 'tour.subscriptions.subscriptionID': entry.subscriptionID });
		await wait(500);
		if (counter === 5) {

			log.error(LOG_FILE_NAME + "writeWhenExists(..) could not find trip containing subscriptionID=" + entry.subscriptionID);
		}
	}

	var notificationContentType = entry.notificationContentType;
	var doc;
	if (notificationContentType === "SUBSCRIPTION_CONFIRMATION") {

		dbFolder = "tour.confirmations";
		doc = await daveTourModel.findOneAndUpdate(
			{ 'tour.subscriptions.subscriptionID': entry.subscriptionID },
			{ $addToSet: { 'tour.confirmations': entry } },
			{ new: true }
		);
	}
	else if (notificationContentType === "ETA_INFO") {

		var eta = {

			relatedTime: entry.notificationPayLoad[0].relatedTime,
			scemid: exists.tour.scemid,
			etaInfos: entry
		}
		doc = await daveTourModel.findOneAndUpdate(
			{ 'tour.subscriptions.subscriptionID': entry.subscriptionID },
			{ $addToSet: { 'tour.eta': eta } },
			{ new: true }
		);
	}
	else {

		doc = await daveTourModel.findOneAndUpdate(
			{ 'tour.subscriptions.subscriptionID': entry.subscriptionID },
			{ $addToSet: { 'tour.states': entry } },
			{ new: true }
		);
	}

	return entry.subscriptionID;
}

const app = express();

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));

/**
 * Process the needed HEADERS
 */
app.use((req, res, next) => {

	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, OPTIONS");
	next();
});

//app.use('/apiTest', appRoutes);

app.post('/api/V1/users', (req, res, next) => {

	const value = req.body;
	const userConf = new UserConfig({

		name: "Value name",
		temp: "value temp"
	});
	userConf.save();
	res.status(201).json({ message: 'POST a USER was successfull' });
});

app.get('/api/V1/users', (req, res, next) => {

	const value = req.body;
	var connection = databaseHandler.getDatabaseConnection("AccountMgmt");
	res.status(201).json({ message: "Toll" });
});

app.get('/api/V1/dave/query', (req, res, next) => {

	log.info(LOG_FILE_NAME + "GET => URL(./api/V1/dave/query) called");
	if (req.query === null || req.query === undefined || req.query.finder === undefined) {

		return res.status(400).json({ message: 'Missing the mandatory database query parameter' });
	}
	if (log.isDebugEnabled) {

		log.debug(LOG_FILE_NAME + "QueryParameters_ " + JSON.stringify(req.query));
		log.debug(LOG_FILE_NAME + "QueryParameters_query=" + req.query.finder);
		log.debug(LOG_FILE_NAME + "QueryParameters_projection=" + req.query.projection);
	}

	var connection = databaseHandler.getDatabaseConnection("Data");
	var daveTourModel = connection.model("DAveTourModel");

	var jsonFinderObject = JSON.parse(req.query.finder);
	var jsonProjectionObject = null;
	if (req.query.projection) {

		jsonProjectionObject = JSON.parse(req.query.projection);
	}

	daveTourModel.find(jsonFinderObject, jsonProjectionObject)
		.then(documents => {

			log.info(LOG_FILE_NAME + "GET => URL(./api/V1/dave/query) returned document count: " + documents.length);
			return res.status(200).json({ message: 'GET: SUCCESS', documents: documents });
		})
	//	res.status(204).json({ message: 'GET: SUCCESS_NO_CONTENT' });
});

/**
 * Abfragen usw. für das Tasks example
 */
app.post('/api/posts', (req, res, next) => {

	const value = req.body;

	resp.status(201).json({ message: 'POST successfull' });
});

app.post('/api/V1/aeolix/publish', (req, res, next) => {

	log.info(LOG_FILE_NAME + "POST => URL(./api/V1/aeolix/publish) called");
	const value = req.body;

	var datasourceId = "5b9f47fce661910005a07de6";
	try {
		myAeolix.publish(datasourceId, req.body);
	}
	catch (e) {

		log.error(LOG_FILE_NAME + "POST=>/api/V1/aeolix/publish: Publish failed: " + e);
		res.status(400).json({ message: 'POST=>/api/V1/aeolix/publish: Publish failed' });
	}

	res.status(201).json({ message: 'POST=>/api/V1/aeolix/publish: Publish was successful' });
});

app.post('/api/V1/aeolix/publishTrip', (req, res, next) => {

	log.info(LOG_FILE_NAME + "POST => URL(./api/V1/aeolix/publishTrip) called");
	const value = req.body;

	try {
		myAeolix.publishTrip(req.body);
	}
	catch (e) {

		log.error(LOG_FILE_NAME + "POST=>/api/V1/aeolix/publish/trip: Publish failed: " + e);
		res.status(400).json({ message: 'POST=>/api/V1/aeolix/publish/trip: Publish failed' });
	}

	res.status(201).json({ message: 'POST=>/api/V1/aeolix/publish/trip: Publish was successful' });
});

/**
 * Das ist ein POST, weil ich einen Body übergeben muss. In diesem BODY befindet sich 
 * die komplette PUBLISH message die Aeolix benötigt.
 * Der Ablauf
 * Channel Publish
 * Channel Received >= delete Aufruf nach DAve => PUBLISH response zu einem Channel(???)
 * Löschen in DAve über dessen Rest API SUCCESS => löschen in unserer DB 
 */
app.post('/api/V1/aeolix/deleteTrip', (req, res, next) => {

	log.info(LOG_FILE_NAME + "POST => URL(./api/V1/aeolix/deleteTrip) called");
	const value = req.body;
	const id2Delete = req.param("id");
	const executor = req.param("executor");

	log.info(LOG_FILE_NAME + `DELETE=>/api/V1/aeolix/deleteTrip: Params: id2Delete=${id2Delete}, executor=${executor}`);
	if (executor === "Aeolix") {

	}
	else if (executor === "DAve") {

	}
	else {

		return res.status(400).json({ message: 'DELETE=>/api/V1/aeolix/deleteTrip: Deleting was successful' });
	}

	return res.status(200).json({ message: `deleteTrip: executed through ${executor}` });
});

/**
 * Diese Route wird dazu verwendet um Trips in "unserer" Datenbank und in DAve zu löschen
 */
app.delete('/api/V1/trip', async (req, res, next) => {

	log.info(LOG_FILE_NAME + "DELETE => URL(./api/V1/trip) called");
	const daveUrl = req.param("daveUrl");
	const daveToken = req.param("daveToken");
	const daveSource = req.param("daveSource");
	const daveScemid = req.param("daveScemid");
	const theHaulierId = req.param("theHaulier");

	/* 	let response = await daveApi.deleteTour(`${daveUrl}?SCEMIDS=${daveScemid}&Token=${daveToken}&Source=${daveSource}`);
		if (response.status === 400) {
	
			return res.status(400).json({ message: `deleteTrip: Tour.SCEMID = ${daveScemid} failed`, response: response.data.responseStatus });
		}
	 */
	/**
	 * Wir löschen zuerst in DAve
	 */
	var daveTourApi = new DAveTourAPI();
	var response = await daveTourApi.deleteTrip(`${daveUrl}?SCEMIDS=${daveScemid}&Token=${daveToken}&Source=${daveSource}`);
	if (response.error === true) {

		return res.status(400).json({ message: `deleteTrip: Tour.SCEMID = ${daveScemid} failed`, response: response.data.responseStatus });
	}

	/**
	 * Und jetzt noch in unserer Datenbank
	 */
	var connection = databaseHandler.getDatabaseConnection("Data");
	var daveTourModel = connection.model("DAveTourModel");
	await daveTourModel.deleteOne({ _id: theHaulierId })
		.then((deletedObject) => {
			var message = `DELETE=>/api/V1/trip: Delete tour id=${theHaulierId} in the MongoDB`;
			if (deletedObject.n === 1) {
				log.info(LOG_FILE_NAME + message + " was successful");
				return res.status(201).json({
					message: message
				});
			}
			else {
				log.info(LOG_FILE_NAME + message + " failed");
				return res.status(400).json({
					message: message
				});
			}
		}).catch(() => {
			return res.status(400).json({ message: `DELETE=>/api/V1/trip: Delete tour id=${theHaulierId} FAILURE` });
		});
});

app.post('/api/V1/aeolix/subscribe', (req, res, next) => {

	/**
	 * ToDo: Überlegen ob hier das Aeolix im Pfad richtig ist.
	 * 
	 * Warum= Ich möchte eine Subscription erzeugen, die kann entweder in Richtung
	 * DAve, Datenbank und oder Aeolix gehen.
	 * Hier müssen wir uns für eine finale Version Gedanken zu unserem Ablauf machen.
	 */
	log.info(LOG_FILE_NAME + "POST => URL(./api/V1/aeolix/subscribe) called");
	//ToDo: var type = JSON.parse(req.query.subscriptionType);
	try {

		//myAeolix.subscribe(req.body, type);
		myAeolix.subscribe(req.body, null);
		return res.status(201).json({ message: 'POST=>/api/V1/aeolix/subscribe: Publish subscription was successful' });
	}
	catch (e) {

		log.error(LOG_FILE_NAME + "POST=>/api/V1/aeolix/subscribe: Publish failed: " + e);
		return res.status(400).json({ message: 'POST=>/api/V1/aeolix/subscribe: Publish failed' });
	}
});

app.post('/api/V1/aeolix/notification', (req, res, next) => {

	log.info(LOG_FILE_NAME + "POST => URL(./api/V1/aeolix/notification) called");

	const notificationReceiverType = req.param("type");
	log.info(LOG_FILE_NAME + "POST=>/api/V1/aeolix/notification: Params=" + req.params);
	if (notificationReceiverType === undefined || notificationReceiverType === null) {

		log.error(LOG_FILE_NAME + "POST=>/api/V1/aeolix/notification: Params=" + req.params);
		return res.status(200).json({ message: 'POST=>/api/V1/aeolix/notification: Error' });
	}

	const theBody = req.body;

	/* 	if (log.isDebugEnabled) {
	
			//ToDo: log.debug(LOG_FILE_NAME + "POST=>/api/V1/aeolix/notification: ClientId=" + theClientID + ", Notification=" + JSON.stringify(theBody));
		}
	 */
	/**
	 * PUBLISH the NOTIFICATION content
	 */
	if (notificationReceiverType === "Service") {

		const serviceID = req.param("serviceID");
		const userID = req.param("userID");
		myAeolix.publishNotification2Service(serviceID, userID, theBody);
		//var service = `&serviceID=${data.typeID}&userID=${forward2UserID}`;
	}
	else if (notificationReceiverType === "Datasource") {

		const datasourceID = req.param("type");
		myAeolix.publish2Datasource(datasourceID, theBody);
		//var datasource = `&datasourceID=${data.data.data.myAeolixId}`;
	}
	else {

	}

	return res.status(200).json({ message: 'POST=>/api/V1/aeolix/notification: SUCCESS' });

	/* 	if (theClientID != null && theClientID != undefined) {
	
			myAeolix.publish2Datasource(theClientID, theBody);
		}
	 */
	var notificationContentType = req.body.notificationContentType;
	/**
	 * Save the NOTIFICATION in our database
	 */
	try {

		log.debug(LOG_FILE_NAME + "POST=>/api/V1/aeolix/notification: Notification=" + JSON.stringify(theBody));

		console.log(`Updating ${theBody.subscriptionID}.`)
		writeWhenExists(theBody)
			.then(id => console.log(`Successfully updated ${id}, NotificationContentType=${notificationContentType}`))
			.catch(err => console.error(`Failed to update ${theBody.subscriptionID}.`, err))

		//return res.status(201).json({ message: 'POST=>/api/V1/aeolix/notification: Publish was successful' });
	}
	catch (e) {

		log.error(LOG_FILE_NAME + "POST=>/api/V1/aeolix/notification: failed with exception");
		return res.status(200).json({ message: 'POST=>/api/V1/aeolix/notification: Exception=' + e.message });
	}

	/**
	 * EmitterHandler 
	 */
	if (notificationContentType === "ETA_INFO") {

		try {
			var channelKey = "fiVnTRtWF3u1_4la0muIf2TR04lsVyhL";
			var channelName = "etainfo";
			var subscriptionID = req.body.subscriptionID; // "02dd83d615464bfeb759a9a8009d9f54";

			//var theBody = `Hallo von nodejs ${version}`;
			var emitterHandler = new EmitterHandler();
			emitterHandler.publishNotification(channelKey, channelName, subscriptionID, theBody);
			//return res.status(201).json({ message: 'POST=>/api/V1/aeolix/notification: Publish was successful' });
		}
		catch (e) {

			//ToDo: Log usw. ändern
			log.error(LOG_FILE_NAME + "POST=>/api/V1/aeolix/notification: ClientId=" + theClientID + " faild with exception");
			return res.status(200).json({ message: 'POST=>/api/V1/aeolix/notification: Exception=' + e.message });
		}
	}
	return res.status(201).json({ message: 'POST=>/api/V1/aeolix/notification: Publish was successful' });
});

app.post('/api/V1/aeolix/position', (req, res, next) => {

	log.info(LOG_FILE_NAME + "POST => URL(./api/V1/aeolix/position) called");
	const value = req.body;

	try {
		myAeolix.position(req.body);
	}
	catch (e) {

		log.error(LOG_FILE_NAME + "POST=>/api/V1/aeolix/position: Publish failed: " + e);
		res.status(400).json({ message: 'POST=>/api/V1/aeolix/position: Publish failed' });
	}

	res.status(201).json({ message: 'POST=>/api/V1/aeolix/position: Publish was successful' });
});

app.post('/api/V1/aeolix/orders', (req, res, next) => {

	log.info(LOG_FILE_NAME + "POST => URL(./api/V1/aeolix/orders) called");
	const value = req.body;

	try {
		myAeolix.order(req.body);
	}
	catch (e) {

		log.error(LOG_FILE_NAME + "POST=>/api/V1/aeolix/order: Publish failed: " + e);
		res.status(400).json({ message: 'POST=>/api/V1/aeolix/order: Publish failed' });
	}

	res.status(201).json({ message: 'POST=>/api/V1/aeolix/order: Publish was successful' });
});

/**
 * Die xServer Inhalte wurden verschoben und getestet => OK
 */
app.use('/api/V1/xServer', xServerRoutes);
app.use('/api/V1/auth', authRoutes);
app.use('/api/V1/th', theHaulierTour);

app.use('/aeolix_temp', aeolixRoutes);
app.use('/dave_temp', daveRoutes);
app.use('/', appRoutes);

module.exports = app;