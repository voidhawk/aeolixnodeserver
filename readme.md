Dieses Projekt enthält den NodeJS Server für TT (Transforming Transport) und Aeolix

Produktive DatasourceId´s
|Name|DatasourceId|Member|
|ETATrip|5b9f47fce661910005a07de6|PTV Group|
|ETAService|5b9b87e8aee25e0005233abe|PTV Group|
|ETAPosition|5b9f4827e661910005a07de7|PTV Group|
|ETASubscription|5b9f4857e661910005a07de8|PTV Group|

Entwicklung DatasourceId´s
|Name|DatasourceId|Member|
|Dev-EtaTrip|5bc6c312dcc514001bd7dd4e|PTV Group|
|Dev-EtaSubscription|5bc6c2fddcc514001bd7dd4d|PTV Group|
|Dev-EtaService|5bc6c2dddcc514001bd7dd4c|PTV Group|
|Dev-EtaPosition|5bc6c332dcc514001bd7dd4f|PTV Group|
|Dev-Notification|5bc6ed8adcc514001bd7dd6a|PTV Group|

Entwicklung ServiceId´s
|Name|ServiceId|Member|
|Dev-Dev_ETAService_ETA|5bf3bba98a84860012bef726|PTV Group|


| Date | Version |Description | Originator |
| 20190201 | 2.1.0 | Subscription are not stored automatically | JSt|
| 20181018 | 1.6.2 | Added rights URL routes  | JSt|
| 20181018 | 1.6.1 | Added tenant URL routes  | JSt|
| 20181018 | 1.6.0 | Update to  MyAeolix SDK 0.3.0 | JSt|
| 20181018 | 1.2.0 | Addded the new MyAeolix SDK (Position for SCEMID) | JSt|
| 20181017 | 1.1.0 | Addded the new MyAeolix SDK (Subscribe to SCEMID) | JSt|
| 20181016 | 1.0.0 | Addded the new MyAeolix SDK (Publish to DatasourceId) | JSt|
